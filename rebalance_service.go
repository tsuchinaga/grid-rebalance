package grid_rebalance

import (
	"math"
)

type IRebalanceService interface {
	IsRebalanceRequired(strategyCode string, price float64) (bool, error)
	Rebalance(strategyCode string, price float64) error
}

type rebalanceService struct {
	strategyStore   IStrategyStore
	orderStore      IOrderStore
	positionService IPositionService
	kabusapi        IKabusAPI
}

// LevelingQuantity - リバランス時の売買枚数
// 負の値なら売り、正の値なら買い
func (s *rebalanceService) LevelingQuantity(cash float64, price float64, quantity float64, tradeUnit float64) float64 {
	// ゼロ除算はできないので、必須の情報がなければ判断できないため0枚を返す
	if price <= 0 || tradeUnit <= 0 {
		return 0
	}

	v := quantity * price                   // ポジション評価額
	q := (cash - v) / 2 / price / tradeUnit // 差額 / 2 で中央を計算し、中央 / price で差額を埋めるまでの株数を計算し、株数 / tradeUnit で何単元かを計算する

	return math.Floor(q) // 正の値のときは0に近づけ、負の値のときは0から遠ざける
}

// IsRebalanceRequired - リバランスが必要かのチェック
func (s *rebalanceService) IsRebalanceRequired(strategyCode string, price float64) (bool, error) {
	// 戦略と現金取得
	strategy, err := s.strategyStore.FindByCode(strategyCode)
	if err != nil {
		return false, err
	}

	// 保有数量取得
	quantity, err := s.positionService.TotalQuantityByStrategyCodeAndSymbolCodeAndSide(strategy.Code, strategy.SymbolCode, strategy.EntrySide)
	if err != nil {
		return false, err
	}

	return s.LevelingQuantity(strategy.Cash, price, quantity, strategy.TradingUnit) != 0, nil
}

// Rebalance - 現金と残高を半分にしてバランスをとれるよう売買する
func (s *rebalanceService) Rebalance(strategyCode string, price float64) error {
	// 戦略と現金取得
	strategy, err := s.strategyStore.FindByCode(strategyCode)
	if err != nil {
		return err
	}

	// 保有数量取得
	quantity, err := s.positionService.TotalQuantityByStrategyCodeAndSymbolCodeAndSide(strategy.Code, strategy.SymbolCode, strategy.EntrySide)
	if err != nil {
		return err
	}

	// 売買数量
	levelingQuantity := s.LevelingQuantity(strategy.Cash, price, quantity, strategy.TradingUnit)
	var tradeType TradeType
	var side Side
	var orderQuantity float64
	var exitPositions []ExitPosition
	if levelingQuantity > 0 {
		tradeType = TradeTypeEntry
		side = strategy.EntrySide
		orderQuantity = levelingQuantity
	} else if levelingQuantity < 0 {
		tradeType = TradeTypeExit
		side = strategy.EntrySide.Turn()
		orderQuantity = levelingQuantity * -1

		holds, err := s.positionService.GetAndHoldQuantity(strategy.Code, strategy.SymbolCode, side, orderQuantity, HoldOrderFirst)
		if err != nil {
			return err
		}
		for _, hp := range holds {
			exitPositions = append(exitPositions, ExitPosition{PositionCode: hp.PositionCode, Quantity: hp.HoldQuantity})
		}
	} else {
		return nil
	}

	order := &Order{
		StrategyCode:         strategy.Code,
		SymbolCode:           strategy.SymbolCode,
		Exchange:             strategy.Exchange,
		OrderKind:            OrderKindRebalance,
		StockMarginTradeType: strategy.StockMarginTradeType,
		ExecutionType:        ExecutionTypeMarket,
		Side:                 side,
		TradeType:            tradeType,
		OrderQuantity:        orderQuantity,
		ExitPositions:        exitPositions,
		AccountType:          strategy.Account.AccountType,
	}
	res, err := s.kabusapi.SendOrder(strategy, order)
	if err != nil {
		// 注文に失敗した場合、拘束したポジションがあれば解放する
		if order.TradeType == TradeTypeExit {
			_ = s.positionService.ReleaseHoldQuantity(order.GetHoldingPositions())
		}
		return err
	}

	order.SetOrderResult(res)
	if !res.Result {
		// 注文に失敗した場合、拘束したポジションがあれば解放する
		if order.TradeType == TradeTypeExit {
			_ = s.positionService.ReleaseHoldQuantity(order.GetHoldingPositions())
		}
		for i, ep := range order.ExitPositions {
			order.ExitPositions[i].ReleasedQuantity = ep.Quantity
		}
	}

	// 注文の保存
	if err := s.orderStore.Insert(order); err != nil {
		return err
	}

	return nil
}
