package grid_rebalance

import "fmt"

// IService - グリッドリバランスサービスのインターフェース
type IService interface {
	RegisterStrategy(strategy RegisterStrategy)
	RemoveStrategy(code string)
	SendRebalanceOrder()
	// 運用資産の増減
	// 約定確認(約定確認とグリッドの整地)
	// 全注文の取消
}

// service - グリッドリバランスサービス
type service struct {
	logger           ILogger
	strategyService  IStrategyService
	symbolService    ISymbolService
	rebalanceService IRebalanceService
}

// RegisterStrategy - 戦略の登録
func (s *service) RegisterStrategy(strategy RegisterStrategy) {
	if err := s.strategyService.Register(s.strategyService.ConvertStrategy(strategy)); err != nil {
		s.logger.Warning(fmt.Errorf("戦略の登録に失敗しました: %w", err))
	}
}

func (s *service) RemoveStrategy(code string) {
	if err := s.strategyService.Remove(code); err != nil {
		s.logger.Warning(fmt.Errorf("戦略の削除に失敗しました: %w", err))
	}
}

// SendRebalanceOrder - リバランスのための注文を送信する
func (s *service) SendRebalanceOrder() {
	// アクティブな戦略一覧取得
	strategies, err := s.strategyService.FindActiveStrategies()
	if err != nil {
		s.logger.Warning(fmt.Errorf("リバランス処理で戦略一覧が取得できなかったのでスキップしました: %w", err))
		return
	}

	for _, strategy := range strategies {
		// 価格情報取得
		symbol, err := s.symbolService.GetSymbol(strategy.SymbolCode, strategy.Exchange)
		if err != nil {
			s.logger.Warning(fmt.Errorf("戦略コード: %sのリバランス処理で価格が取れなかったのでスキップしました: %w", strategy.Code, err))
			continue
		}
		// リバランス注文を入れる
		if err := s.rebalanceService.Rebalance(strategy.Code, (symbol.BidPrice+symbol.AskPrice)/2); err != nil {
			s.logger.Warning(fmt.Errorf("リバランス注文の送信に失敗しました: %w", err))
			continue
		}
	}
}
