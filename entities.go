package grid_rebalance

import (
	"encoding/json"
	"time"
)

// Strategy - 戦略
// 売買対象や、グリッドの設定などをもつ
type Strategy struct {
	Code                 string               // ID
	SymbolCode           string               // 銘柄コード
	Exchange             Exchange             // 市場
	StockMarginTradeType StockMarginTradeType // 現物・信用取引区分
	EntrySide            Side                 // エントリー時の売買方向
	GridWidth            int                  // グリッド幅
	GridNum              int                  // グリッド数(片側)
	Quantity             float64              // 1グリッドの注文数
	AllottedAsset        float64              // 割当資産額 ユーザーが設定した金額で変動しない
	ManagementAsset      float64              // 運用資産額 日々の取引での増減を加味した運用可能な資産
	Cash                 float64              // 現金 システムによって更新される
	TradingUnit          float64              // 売買単位
	Active               bool                 // 有効か無効か
	Account              Account              // 口座情報
}

func (e *Strategy) String() string {
	if b, err := json.Marshal(e); err != nil {
		return err.Error()
	} else {
		return string(b)
	}
}

// Order - 注文
type Order struct {
	Code                 string               // 証券会社注文コード
	StrategyCode         string               // 戦略コード
	SymbolCode           string               // 銘柄コード
	Exchange             Exchange             // 市場
	OrderKind            OrderKind            // 注文種別
	StockMarginTradeType StockMarginTradeType // 現物・信用取引区分
	ExecutionType        ExecutionType        // 執行条件
	Status               OrderStatus          // 注文状態
	Side                 Side                 // 売買方向
	TradeType            TradeType            // 取引種別
	Price                float64              // 指値価格
	OrderQuantity        float64              // 注文数
	ContractQuantity     float64              // 約定数
	ExitPositions        []ExitPosition       // エグジットポジション
	OrderDateTime        time.Time            // 注文日時
	ContractDateTime     time.Time            // 約定日時
	CancelDateTime       time.Time            // 取消日時
	ErrorCode            string               // エラーコード
	AccountType          AccountType          // 口座種別
}

func (e *Order) String() string {
	if b, err := json.Marshal(e); err != nil {
		return err.Error()
	} else {
		return string(b)
	}
}

// SetOrderResult - 注文結果を注文の状態に反映する
func (e *Order) SetOrderResult(result *OrderResult) {
	if result == nil {
		return
	}
	e.Code = result.Code
	e.ErrorCode = result.ErrorCode
	if result.Result {
		e.Status = OrderStatusInOrder
	} else {
		e.Status = OrderStatusError
	}
	e.OrderDateTime = result.OrderDateTime
}

// GetHoldingPositions - 拘束中数量を返す
func (e *Order) GetHoldingPositions() []HoldPosition {
	res := make([]HoldPosition, 0)

	for _, ep := range e.ExitPositions {
		q := ep.Quantity - ep.ReleasedQuantity
		if q <= 0 {
			continue
		}
		res = append(res, HoldPosition{PositionCode: ep.PositionCode, HoldQuantity: q})
	}

	return res
}

// Position - ポジション
type Position struct {
	Code                 string               // 約定コード = ポジションコード
	OrderCode            string               // 注文コード
	StrategyCode         string               // 戦略コード
	SymbolCode           string               // 銘柄コード
	Side                 Side                 // 売買方向
	StockMarginTradeType StockMarginTradeType // 現物・信用取引区分
	Price                float64              // 約定値
	OwnedQuantity        float64              // 保有ポジション数
	HoldQuantity         float64              // 拘束ポジション数
	TradingUnit          float64              // 売買単位
	ContractDateTime     time.Time            // 約定日時
}

func (e *Position) String() string {
	if b, err := json.Marshal(e); err != nil {
		return err.Error()
	} else {
		return string(b)
	}
}
