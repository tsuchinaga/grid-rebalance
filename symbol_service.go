package grid_rebalance

// ISymbolService - 銘柄のサービスのインターフェース
type ISymbolService interface {
	RegisterSymbol(symbolCode string, exchange Exchange) error
	GetSymbol(symbolCode string, exchange Exchange) (*Symbol, error)
}

// symbolService - 銘柄のサービス
type symbolService struct {
	kabusAPI IKabusAPI
}

// RegisterSymbol - 情報取得対象の銘柄を事前に登録する処理
func (s *symbolService) RegisterSymbol(symbolCode string, exchange Exchange) error {
	return s.kabusAPI.RegisterSymbol(symbolCode, exchange)
}

// GetSymbol - 銘柄情報の取得
func (s *symbolService) GetSymbol(symbolCode string, exchange Exchange) (*Symbol, error) {
	// TODO エラーなく情報が取れない場合があるっぽいので、どの項目が欠けるかを見てからリトライするようにする
	return s.kabusAPI.GetSymbol(symbolCode, exchange)
}
