package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

type testOrderStore struct {
	IOrderStore
	Insert1       error
	InsertHistory []interface{}
}

func (t *testOrderStore) Insert(order *Order) error {
	if t.InsertHistory == nil {
		t.InsertHistory = make([]interface{}, 0)
	}
	t.InsertHistory = append(t.InsertHistory, order)
	return t.Insert1
}

func Test_orderStore_FindByCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Order
		arg1    string
		want1   *Order
		want2   error
	}{
		{name: "codeのデータがなければerror",
			dataset: []*Order{
				{Code: "order-0010", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			arg1:  "no-code",
			want1: nil,
			want2: ErrDataNotFound},
		{name: "codeのデータがあればorderに詰めて返す",
			dataset: []*Order{
				{Code: "order-0010", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			arg1:  "order-0011",
			want1: &Order{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into orders values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &orderStore{store: db}
			got1, got2 := store.FindByCode(test.arg1)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_orderStore_FindActiveOrders(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Order
		want1   []*Order
		want2   error
	}{
		{name: "対象のデータがなければなにもしない",
			dataset: []*Order{
				{Code: "order-0010", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInCancel, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusCanceled, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			want1: []*Order{},
			want2: nil},
		{name: "対象のデータがあればorderのsliceに詰めて返す",
			dataset: []*Order{
				{Code: "order-0010", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			want1: []*Order{
				{Code: "order-0011", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into orders values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &orderStore{store: db}
			got1, got2 := store.FindActiveOrders()
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_orderStore_FindActiveOrdersByStrategyCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Order
		arg     string
		want1   []*Order
		want2   error
	}{
		{name: "対象のステータスがなければなにもしない",
			dataset: []*Order{
				{Code: "order-0010", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInCancel, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusCanceled, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			arg:   "1475",
			want1: []*Order{},
			want2: nil},
		{name: "対象の戦略がなければなにもしない",
			dataset: []*Order{
				{Code: "order-0010", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			arg:   "0000",
			want1: []*Order{},
			want2: nil},
		{name: "対象のデータがあればorderのsliceに詰めて返す",
			dataset: []*Order{
				{Code: "order-0010", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindRebalance, Status: OrderStatusDone, Side: SideBuy, Price: 0, OrderQuantity: 10, ContractQuantity: 10, OrderDateTime: time.Date(2021, 10, 8, 9, 0, 0, 0, time.Local), ContractDateTime: time.Date(2021, 10, 8, 9, 0, 10, 0, time.Local), CancelDateTime: time.Time{}},
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			arg: "1475",
			want1: []*Order{
				{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
				{Code: "order-0012", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideBuy, Price: 2016, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into orders values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &orderStore{store: db}
			got1, got2 := store.FindActiveOrdersByStrategyCode(test.arg)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_orderStore_Insert(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name       string
		dataset    []*Order
		arg        *Order
		want       error
		wantOrders []*Order
	}{
		{name: "insertしたデータが取れる",
			dataset:    []*Order{},
			arg:        &Order{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			want:       nil,
			wantOrders: []*Order{{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}}}},
		{name: "uniqueキー制約に引っかかったらinsert出来ない",
			dataset:    []*Order{{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}}},
			arg:        &Order{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}},
			want:       ErrDataDuplicated,
			wantOrders: []*Order{{Code: "order-0011", StrategyCode: "1475", SymbolCode: "1475", OrderKind: OrderKindGrid, Status: OrderStatusInOrder, Side: SideSell, Price: 2020, OrderQuantity: 2, ContractQuantity: 0, OrderDateTime: time.Date(2021, 10, 8, 10, 0, 0, 0, time.Local), ContractDateTime: time.Time{}, CancelDateTime: time.Time{}}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into orders values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &orderStore{store: db}
			got := store.Insert(test.arg)

			orders := make([]*Order, 0)
			res, _ := db.Query("select * from orders")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var order Order
				_ = document.StructScan(d, &order)
				orders = append(orders, &order)
				return nil
			})

			if !reflect.DeepEqual(test.wantOrders, orders) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}
