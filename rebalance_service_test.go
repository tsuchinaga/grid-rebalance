package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"
	"time"
)

type testRebalanceService struct {
	IRebalanceService
	Rebalance1       error
	RebalanceCount   int
	RebalanceHistory []interface{}
}

func (t *testRebalanceService) Rebalance(strategyCode string, price float64) error {
	if t.RebalanceHistory == nil {
		t.RebalanceHistory = make([]interface{}, 0)
	}
	t.RebalanceHistory = append(t.RebalanceHistory, strategyCode)
	t.RebalanceHistory = append(t.RebalanceHistory, price)
	t.RebalanceCount++
	return t.Rebalance1
}

func Test_rebalanceService_IsRebalanceRequired(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name            string
		strategyStore   *testStrategyStore
		positionService *testPositionService
		arg1            string
		arg2            float64
		want1           bool
		want2           error
	}{
		{name: "strategyが取れなければエラー",
			strategyStore: &testStrategyStore{FindByCode1: nil, FindByCode2: ErrDataNotFound},
			arg1:          "strategy-1475",
			arg2:          2000,
			want1:         false,
			want2:         ErrDataNotFound},
		{name: "positionがnot found以外の理由で取れなければエラー",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475"}, FindByCode2: nil},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 0, TotalQuantityByStrategyCodeAndSymbolCodeAndSide2: ErrUnknown},
			arg1:            "strategy-1475",
			arg2:            2000,
			want1:           false,
			want2:           ErrUnknown},
		{name: "現金が10_000で、保有株数が0株で、基準価格が5_000ならリバランスする",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Cash: 10_000, TradingUnit: 1}, FindByCode2: nil},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 0, TotalQuantityByStrategyCodeAndSymbolCodeAndSide2: nil},
			arg1:            "strategy-1475",
			arg2:            5_000,
			want1:           true,
			want2:           nil},
		{name: "現金が10_000で、保有株数が2株で、基準価格が5_000ならリバランスしない",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Cash: 10_000, TradingUnit: 1}, FindByCode2: nil},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 2, TotalQuantityByStrategyCodeAndSymbolCodeAndSide2: nil},
			arg1:            "strategy-1475",
			arg2:            5_000,
			want1:           false,
			want2:           nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := rebalanceService{strategyStore: test.strategyStore, positionService: test.positionService}
			got1, got2 := service.IsRebalanceRequired(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_rebalanceService_LevelingQuantity(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 float64
		arg2 float64
		arg3 float64
		arg4 float64
		want float64
	}{
		{name: "priceが0なら何もせず0を返す", arg1: 100_000, arg2: 0, arg3: 50, arg4: 1, want: 0},
		{name: "tradeUnitが0なら何もせず0を返す", arg1: 100_000, arg2: 2_000, arg3: 50, arg4: 0, want: 0},
		{name: "cashとarg2*arg3が同じ場合、調整が不要なので0", arg1: 100_000, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash < arg1*arg3 で、差が 1 なら、-1", arg1: 99_999, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price/2 - 1 なら、-1", arg1: 99_001, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price/2 なら、-1", arg1: 99_000, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price/2 + 1 なら、-1", arg1: 98_999, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price - 1 なら、-1", arg1: 98_001, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price なら、-1", arg1: 98_000, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price + 1 なら、-1", arg1: 97_999, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price*2 - 1 なら、-1", arg1: 96_001, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price*2 なら、-1", arg1: 96_000, arg2: 2_000, arg3: 50, arg4: 1, want: -1},
		{name: "Cash < arg1*arg3 で、差が price*2 + 1 なら、-2", arg1: 95_999, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*3 - 1 なら、-2", arg1: 94_001, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*3 なら、-2", arg1: 94_000, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*3 + 1 なら、-2", arg1: 93_999, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*4 - 1 なら、-2", arg1: 92_001, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*4 なら、-2", arg1: 92_000, arg2: 2_000, arg3: 50, arg4: 1, want: -2},
		{name: "Cash < arg1*arg3 で、差が price*4 + 1 なら、-3", arg1: 91_999, arg2: 2_000, arg3: 50, arg4: 1, want: -3},
		{name: "Cash > arg1*arg3 で、差が 1 なら、0", arg1: 100_001, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price/2 - 1 なら、0", arg1: 100_999, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price/2 なら、1", arg1: 101_000, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price/2 + 1 なら、1", arg1: 101_001, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price - 1 なら、1", arg1: 101_999, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price なら、1", arg1: 102_000, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price + 1 なら、1", arg1: 102_001, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price*2 - 1 なら、1", arg1: 103_999, arg2: 2_000, arg3: 50, arg4: 1, want: 0},
		{name: "Cash > arg1*arg3 で、差が price*2 なら、1", arg1: 104_000, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash < arg1*arg3 で、差が price*2 + 1 なら、2", arg1: 104_001, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash > arg1*arg3 で、差が price*3 - 1 なら、1", arg1: 105_999, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash > arg1*arg3 で、差が price*3 なら、1", arg1: 106_000, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash < arg1*arg3 で、差が price*3 + 1 なら、1", arg1: 106_001, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash > arg1*arg3 で、差が price*4 - 1 なら、1", arg1: 107_999, arg2: 2_000, arg3: 50, arg4: 1, want: 1},
		{name: "Cash > arg1*arg3 で、差が price*4 なら、2", arg1: 108_000, arg2: 2_000, arg3: 50, arg4: 1, want: 2},
		{name: "Cash < arg1*arg3 で、差が price*4 + 1 なら、2", arg1: 108_001, arg2: 2_000, arg3: 50, arg4: 1, want: 2},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &rebalanceService{}
			got := service.LevelingQuantity(test.arg1, test.arg2, test.arg3, test.arg4)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_rebalanceService_Rebalance(t *testing.T) {
	t.Parallel()
	tests := []struct {
		strategyStore                  *testStrategyStore
		orderStore                     *testOrderStore
		positionService                *testPositionService
		kabusapi                       *testKabusAPI
		name                           string
		arg1                           string
		arg2                           float64
		want                           error
		wantSendOrderHistory           []interface{}
		wantOrderInsertHistory         []interface{}
		wantGetAndHoldQuantity         []interface{}
		wantReleaseHoldQuantityHistory []interface{}
	}{
		{name: "戦略が取れなければエラー",
			strategyStore:   &testStrategyStore{FindByCode2: ErrDataNotFound},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{},
			kabusapi:        &testKabusAPI{},
			arg1:            "strategy-1475",
			want:            ErrDataNotFound},
		{name: "ポジション数量が取れなければエラー",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475"}},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide2: ErrUnknown},
			kabusapi:        &testKabusAPI{},
			arg1:            "strategy-1475",
			want:            ErrUnknown},
		{name: "調整する必要がなければ何もせず終了",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Cash: 300_000, TradingUnit: 1}},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 150},
			kabusapi:        &testKabusAPI{},
			arg1:            "strategy-1475",
			arg2:            2000,
			want:            nil},
		{name: "注文送信でエラーがあったらエラー",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 148},
			kabusapi:        &testKabusAPI{SendOrder2: ErrUnknown},
			arg1:            "strategy-1475",
			arg2:            2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					ExitPositions:        nil,
					AccountType:          AccountTypeSpecific,
				},
			},
			want: ErrUnknown},
		{name: "買い注文に成功してもinsertに失敗したらエラー",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:      &testOrderStore{Insert1: ErrUnknown},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 148},
			kabusapi: &testKabusAPI{SendOrder1: &OrderResult{
				Result:        true,
				Code:          "order-code-1",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
			}},
			arg1: "strategy-1475",
			arg2: 2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "0",
					AccountType:          AccountTypeSpecific,
				},
			},
			want: ErrUnknown,
			wantOrderInsertHistory: []interface{}{
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "0",
					AccountType:          AccountTypeSpecific,
				}}},
		{name: "買い注文に失敗したらエラー注文を保存して終了",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 148},
			kabusapi: &testKabusAPI{SendOrder1: &OrderResult{
				Result:        false,
				Code:          "",
				ErrorCode:     "4",
				OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
			}},
			arg1: "strategy-1475",
			arg2: 2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusError,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "4",
					AccountType:          AccountTypeSpecific,
				},
			},
			want: nil,
			wantOrderInsertHistory: []interface{}{
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusError,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "4",
					AccountType:          AccountTypeSpecific,
				}}},
		{name: "買い注文に成功したら保存して終了",
			strategyStore:   &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:      &testOrderStore{},
			positionService: &testPositionService{TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 148},
			kabusapi: &testKabusAPI{SendOrder1: &OrderResult{
				Result:        true,
				Code:          "order-code-1",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
			}},
			arg1: "strategy-1475",
			arg2: 2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "0",
					AccountType:          AccountTypeSpecific,
				},
			},
			want: nil,
			wantOrderInsertHistory: []interface{}{
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideBuy,
					TradeType:            TradeTypeEntry,
					OrderQuantity:        1,
					OrderDateTime:        time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:            "0",
					AccountType:          AccountTypeSpecific,
				}}},
		{name: "売り注文がエラーなら拘束したポジションを解放してエラーを返して終了",
			strategyStore: &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:    &testOrderStore{},
			positionService: &testPositionService{
				TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 152,
				GetAndHoldQuantity1: []HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 4}, {PositionCode: "position-code-2", HoldQuantity: 4}}},
			kabusapi: &testKabusAPI{SendOrder2: ErrUnknown},
			arg1:     "strategy-1475",
			arg2:     2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Side:                 SideSell,
					TradeType:            TradeTypeExit,
					OrderQuantity:        1,
					ExitPositions: []ExitPosition{
						{PositionCode: "position-code-1", Quantity: 4, ReleasedQuantity: 0},
						{PositionCode: "position-code-2", Quantity: 4, ReleasedQuantity: 0},
					},
					AccountType: AccountTypeSpecific,
				},
			},
			want:                           ErrUnknown,
			wantGetAndHoldQuantity:         []interface{}{"strategy-1475", "1475", SideSell, 1.0, HoldOrderFirst},
			wantReleaseHoldQuantityHistory: []interface{}{[]HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 4}, {PositionCode: "position-code-2", HoldQuantity: 4}}}},
		{name: "売り注文に失敗したら拘束したポジションを解放し、エラー注文を保存して終了",
			strategyStore: &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:    &testOrderStore{},
			positionService: &testPositionService{
				TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 152,
				GetAndHoldQuantity1: []HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 4}, {PositionCode: "position-code-2", HoldQuantity: 4}}},
			kabusapi: &testKabusAPI{SendOrder1: &OrderResult{
				Result:        false,
				Code:          "",
				ErrorCode:     "4",
				OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
			}},
			arg1: "strategy-1475",
			arg2: 2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusError,
					Side:                 SideSell,
					TradeType:            TradeTypeExit,
					OrderQuantity:        1,
					ExitPositions: []ExitPosition{
						{PositionCode: "position-code-1", Quantity: 4, ReleasedQuantity: 4},
						{PositionCode: "position-code-2", Quantity: 4, ReleasedQuantity: 4},
					},
					OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:     "4",
					AccountType:   AccountTypeSpecific,
				},
			},
			want: nil,
			wantOrderInsertHistory: []interface{}{
				&Order{
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusError,
					Side:                 SideSell,
					TradeType:            TradeTypeExit,
					OrderQuantity:        1,
					ExitPositions: []ExitPosition{
						{PositionCode: "position-code-1", Quantity: 4, ReleasedQuantity: 4},
						{PositionCode: "position-code-2", Quantity: 4, ReleasedQuantity: 4},
					},
					OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:     "4",
					AccountType:   AccountTypeSpecific,
				}},
			wantGetAndHoldQuantity:         []interface{}{"strategy-1475", "1475", SideSell, 1.0, HoldOrderFirst},
			wantReleaseHoldQuantityHistory: []interface{}{[]HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 4}, {PositionCode: "position-code-2", HoldQuantity: 4}}}},
		{name: "売り注文に成功したら保存して終了",
			strategyStore: &testStrategyStore{FindByCode1: &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}}},
			orderStore:    &testOrderStore{},
			positionService: &testPositionService{
				TotalQuantityByStrategyCodeAndSymbolCodeAndSide1: 152,
				GetAndHoldQuantity1: []HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 4}, {PositionCode: "position-code-2", HoldQuantity: 4}}},
			kabusapi: &testKabusAPI{SendOrder1: &OrderResult{
				Result:        true,
				Code:          "order-code-1",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
			}},
			arg1: "strategy-1475",
			arg2: 2000,
			wantSendOrderHistory: []interface{}{
				&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, Cash: 300_000, TradingUnit: 1, EntrySide: SideBuy, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideSell,
					TradeType:            TradeTypeExit,
					OrderQuantity:        1,
					ExitPositions: []ExitPosition{
						{PositionCode: "position-code-1", Quantity: 4, ReleasedQuantity: 0},
						{PositionCode: "position-code-2", Quantity: 4, ReleasedQuantity: 0},
					},
					OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:     "0",
					AccountType:   AccountTypeSpecific,
				},
			},
			want: nil,
			wantOrderInsertHistory: []interface{}{
				&Order{
					Code:                 "order-code-1",
					StrategyCode:         "strategy-1475",
					SymbolCode:           "1475",
					Exchange:             ExchangeToushou,
					OrderKind:            OrderKindRebalance,
					StockMarginTradeType: StockMarginTradeTypeStock,
					ExecutionType:        ExecutionTypeMarket,
					Status:               OrderStatusInOrder,
					Side:                 SideSell,
					TradeType:            TradeTypeExit,
					OrderQuantity:        1,
					ExitPositions: []ExitPosition{
						{PositionCode: "position-code-1", Quantity: 4, ReleasedQuantity: 0},
						{PositionCode: "position-code-2", Quantity: 4, ReleasedQuantity: 0},
					},
					OrderDateTime: time.Date(2021, 10, 15, 10, 0, 0, 0, time.Local),
					ErrorCode:     "0",
					AccountType:   AccountTypeSpecific,
				}},
			wantGetAndHoldQuantity: []interface{}{"strategy-1475", "1475", SideSell, 1.0, HoldOrderFirst}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &rebalanceService{
				strategyStore:   test.strategyStore,
				orderStore:      test.orderStore,
				positionService: test.positionService,
				kabusapi:        test.kabusapi,
			}
			got := service.Rebalance(test.arg1, test.arg2)
			if !errors.Is(got, test.want) ||
				!reflect.DeepEqual(test.wantSendOrderHistory, test.kabusapi.SendOrderHistory) ||
				!reflect.DeepEqual(test.wantOrderInsertHistory, test.orderStore.InsertHistory) ||
				!reflect.DeepEqual(test.wantGetAndHoldQuantity, test.positionService.GetAndHoldQuantityHistory) ||
				!reflect.DeepEqual(test.wantReleaseHoldQuantityHistory, test.positionService.ReleaseHoldQuantityHistory) {
				t.Errorf("%s error\nresult: %+v, %+v, %+v, %+v, %+v\nwant: %+v, %+v, %+v, %+v, %+v\ngot: %+v, %+v, %+v, %+v, %+v\n", t.Name(),
					!errors.Is(got, test.want),
					!reflect.DeepEqual(test.wantSendOrderHistory, test.kabusapi.SendOrderHistory),
					!reflect.DeepEqual(test.wantOrderInsertHistory, test.orderStore.InsertHistory),
					!reflect.DeepEqual(test.wantGetAndHoldQuantity, test.positionService.GetAndHoldQuantityHistory),
					!reflect.DeepEqual(test.wantReleaseHoldQuantityHistory, test.positionService.ReleaseHoldQuantityHistory),
					test.want, test.wantSendOrderHistory, test.wantOrderInsertHistory, test.wantGetAndHoldQuantity, test.wantReleaseHoldQuantityHistory,
					got, test.kabusapi.SendOrderHistory, test.orderStore.InsertHistory, test.positionService.GetAndHoldQuantityHistory, test.positionService.ReleaseHoldQuantityHistory)
			}
		})
	}
}
