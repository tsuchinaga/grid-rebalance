package grid_rebalance

import (
	"reflect"
	"testing"
)

func Test_NewService(t *testing.T) {
	t.Parallel()
	want := &service{
		logger: &logger{},
		strategyService: &strategyService{
			strategyStore: &strategyStore{store: store},
			kabusAPI:      &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
		symbolService: &symbolService{
			kabusAPI: &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
		rebalanceService: &rebalanceService{
			strategyStore: &strategyStore{store: store},
			orderStore:    &orderStore{store: store},
			positionService: &positionService{
				positionStore: &positionStore{store: store},
			},
			kabusapi: &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
	}
	got := NewService()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), want, got)
	}
}
