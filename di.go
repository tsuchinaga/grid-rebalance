package grid_rebalance

import (
	"context"
	"log"

	"gitlab.com/tsuchinaga/kabus-grpc-server/kabuspb"
	"google.golang.org/grpc"
)

var kabuspbClient kabuspb.KabusServiceClient

func init() {
	conn, err := grpc.DialContext(context.Background(), "localhost:18082", grpc.WithInsecure())
	if err != nil {
		log.Fatalln(err)
	}
	kabuspbClient = kabuspb.NewKabusServiceClient(conn)
}

// NewService - グリッドリバランスのサービスを返す
func NewService() IService {
	return &service{
		logger: &logger{},
		strategyService: &strategyService{
			strategyStore: &strategyStore{store: store},
			kabusAPI:      &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
		symbolService: &symbolService{
			kabusAPI: &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
		rebalanceService: &rebalanceService{
			strategyStore: &strategyStore{store: store},
			orderStore:    &orderStore{store: store},
			positionService: &positionService{
				positionStore: &positionStore{store: store},
			},
			kabusapi: &kabusAPI{clock: &clock{}, kabucom: kabuspbClient},
		},
	}
}
