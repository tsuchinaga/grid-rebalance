package grid_rebalance

import "fmt"

// IStrategyService - 戦略サービスのインターフェース
type IStrategyService interface {
	ConvertStrategy(request RegisterStrategy) *Strategy
	Register(strategy *Strategy) error
	Remove(code string) error
	FindActiveStrategies() ([]*Strategy, error)
}

// strategyService - 戦略サービス
type strategyService struct {
	strategyStore IStrategyStore
	kabusAPI      IKabusAPI
}

// ConvertStrategy - 戦略登録リクエストから戦略に変換する
func (s *strategyService) ConvertStrategy(request RegisterStrategy) *Strategy {
	return &Strategy{
		Code:                 request.Code,
		SymbolCode:           request.SymbolCode,
		Exchange:             request.Exchange,
		StockMarginTradeType: request.StockMarginTradeType,
		EntrySide:            request.EntrySide,
		GridWidth:            request.GridWidth,
		GridNum:              request.GridNum,
		Quantity:             request.Quantity,
		AllottedAsset:        request.AllottedAsset,
		ManagementAsset:      request.AllottedAsset,
		Cash:                 request.AllottedAsset,
		TradingUnit:          0,
		Active:               true,
		Account: Account{
			Password:    request.OrderPassword,
			AccountType: request.AccountType,
		},
	}
}

// Register - 戦略の登録
func (s *strategyService) Register(strategy *Strategy) error {
	// 引数がnil
	if strategy == nil {
		return ErrNilArguments
	}

	// codeが空でない
	if strategy.Code == "" {
		return fmt.Errorf("コードに有効な値が設定されていません: %w", ErrStrategyValidation)
	}

	// codeに重複がない
	if s.strategyStore.IsExistsByCode(strategy.Code) {
		return fmt.Errorf("登録済みの戦略コードです: %w", ErrStrategyValidation)
	}

	// 銘柄・市場が存在する
	symbol, err := s.kabusAPI.GetSymbol(strategy.SymbolCode, strategy.Exchange)
	if err != nil {
		return err
	}
	strategy.TradingUnit = symbol.TradingUnit

	// 現物・信用区分が指定されている
	if strategy.StockMarginTradeType == StockMarginTradeTypeUnspecified {
		return fmt.Errorf("現物・信用区分に有効な値が設定されていません: %w", ErrStrategyValidation)
	}

	// エントリー時の方向が指定されていて、現物・信用区分と矛盾していない
	if strategy.EntrySide == SideUnspecified {
		return fmt.Errorf("エントリー方向に有効な値が設定されていません: %w", ErrStrategyValidation)
	}
	if strategy.StockMarginTradeType == StockMarginTradeTypeStock && strategy.EntrySide != SideBuy {
		return fmt.Errorf("現物のエントリー方向は買いのみ可能です: %w", ErrStrategyValidation)
	}

	// グリッド幅に1以上の数値が指定されている
	if strategy.GridWidth < 1 {
		return fmt.Errorf("グリッド幅は0より大きな値のみ可能です: %w", ErrStrategyValidation)
	}

	// グリッド数に1以上の数値が指定されている
	if strategy.GridNum < 1 {
		return fmt.Errorf("グリッド数は1以上の値のみ可能です: %w", ErrStrategyValidation)
	}

	// 1グリッドの注文数に1以上の数値が指定されている
	if strategy.Quantity < 1 {
		return fmt.Errorf("1グリッドの注文数は1以上の値のみ可能です: %w", ErrStrategyValidation)
	}

	// 割当資産額に0より大きな数値が指定されている
	if strategy.AllottedAsset <= 0 {
		return fmt.Errorf("割当資産額は0より大きな値のみ可能です: %w", ErrStrategyValidation)
	}

	// 注文パスワードが指定されている
	if strategy.Account.Password == "" {
		return fmt.Errorf("注文パスワードに有効な値が設定されていません: %w", ErrStrategyValidation)
	}

	// 口座種別が指定されている
	if strategy.Account.AccountType == AccountTypeUnspecified {
		return fmt.Errorf("口座種別に有効な値が設定されていません: %w", ErrStrategyValidation)
	}

	return s.strategyStore.Insert(strategy)
}

// Remove - 戦略をcodeで指定して削除
func (s *strategyService) Remove(code string) error {
	if !s.strategyStore.IsExistsByCode(code) {
		return nil
	}
	return s.strategyStore.Delete(code)
}

// FindActiveStrategies - 有効な戦略を取り出す
func (s *strategyService) FindActiveStrategies() ([]*Strategy, error) {
	return s.strategyStore.FindActiveStrategies()
}
