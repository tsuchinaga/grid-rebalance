package grid_rebalance

import (
	"context"
	"strconv"

	"gitlab.com/tsuchinaga/kabus-grpc-server/kabuspb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// IKabusAPI - kabuステーションAPIのインターフェース
type IKabusAPI interface {
	SendOrder(strategy *Strategy, order *Order) (*OrderResult, error)
	RegisterSymbol(symbolCode string, exchange Exchange) error
	GetSymbol(symbolCode string, exchange Exchange) (*Symbol, error)
}

// kabusAPI - kabuステーションAPI
type kabusAPI struct {
	clock   IClock
	kabucom kabuspb.KabusServiceClient
}

// sideTo - Sideをkabus用に変換
func (k *kabusAPI) sideTo(side Side) kabuspb.Side {
	switch side {
	case SideBuy:
		return kabuspb.Side_SIDE_BUY
	case SideSell:
		return kabuspb.Side_SIDE_SELL
	}
	return kabuspb.Side_SIDE_UNSPECIFIED
}

// exchangeTo - Exchangeをkabus用に変換
func (k *kabusAPI) exchangeTo(exchange Exchange) kabuspb.Exchange {
	switch exchange {
	case ExchangeToushou:
		return kabuspb.Exchange_EXCHANGE_TOUSHOU
	}
	return kabuspb.Exchange_EXCHANGE_UNSPECIFIED
}

// exchangeFrom - Exchangeをkabus用から変換
func (k *kabusAPI) exchangeFrom(exchange kabuspb.Exchange) Exchange {
	switch exchange {
	case kabuspb.Exchange_EXCHANGE_TOUSHOU:
		return ExchangeToushou
	}
	return ExchangeUnspecified
}

// accountTypeTo - AccountTypeをkabus用に変換
func (k *kabusAPI) accountTypeTo(accountType AccountType) kabuspb.AccountType {
	switch accountType {
	case AccountTypeGeneral:
		return kabuspb.AccountType_ACCOUNT_TYPE_GENERAL
	case AccountTypeSpecific:
		return kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC
	case AccountTypeCorporation:
		return kabuspb.AccountType_ACCOUNT_TYPE_CORPORATION
	}
	return kabuspb.AccountType_ACCOUNT_TYPE_UNSPECIFIED
}

// orderTypeTo - OrderTypeをkabus用に変換
func (k *kabusAPI) orderTypeTo(executionType ExecutionType) kabuspb.StockOrderType {
	switch executionType {
	case ExecutionTypeMarket:
		return kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO
	case ExecutionTypeLimit:
		return kabuspb.StockOrderType_STOCK_ORDER_TYPE_LO
	}
	return kabuspb.StockOrderType_STOCK_ORDER_TYPE_UNSPECIFIED
}

func (k *kabusAPI) tradeTypeTo(tradeType TradeType) kabuspb.TradeType {
	switch tradeType {
	case TradeTypeEntry:
		return kabuspb.TradeType_TRADE_TYPE_ENTRY
	case TradeTypeExit:
		return kabuspb.TradeType_TRADE_TYPE_EXIT
	}
	return kabuspb.TradeType_TRADE_TYPE_UNSPECIFIED
}

func (k *kabusAPI) marginTradeTypeTo(stockMarginTradeType StockMarginTradeType) kabuspb.MarginTradeType {
	switch stockMarginTradeType {
	case StockMarginTradeTypeMarginSystem:
		return kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_SYSTEM
	case StockMarginTradeTypeMarginLong:
		return kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_LONG
	case StockMarginTradeTypeMarginDay:
		return kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_DAY
	}
	return kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_UNSPECIFIED
}

func (k *kabusAPI) closePositionsTo(closePositions []ExitPosition) []*kabuspb.ClosePosition {
	if closePositions == nil {
		return nil
	}

	res := make([]*kabuspb.ClosePosition, len(closePositions))
	for i, cp := range closePositions {
		res[i] = &kabuspb.ClosePosition{ExecutionId: cp.PositionCode, Quantity: cp.Quantity}
	}
	return res
}

// SendOrder - 注文(現物・信用)
func (k *kabusAPI) SendOrder(strategy *Strategy, order *Order) (*OrderResult, error) {
	if strategy == nil || order == nil {
		return nil, ErrNilArguments
	}

	var res *kabuspb.OrderResponse
	var err error
	if order.StockMarginTradeType == StockMarginTradeTypeStock {
		res, err = k.kabucom.SendStockOrder(context.Background(), &kabuspb.SendStockOrderRequest{
			Password:     strategy.Account.Password,
			SymbolCode:   order.SymbolCode,
			Exchange:     kabuspb.StockExchange(k.exchangeTo(order.Exchange)),
			Side:         k.sideTo(order.Side),
			DeliveryType: kabuspb.DeliveryType_DELIVERY_TYPE_CASH,      // お預かり金 多分固定で大丈夫
			FundType:     kabuspb.FundType_FUND_TYPE_SUBSTITUTE_MARGIN, // 信用代用 多分固定で大丈夫
			AccountType:  k.accountTypeTo(order.AccountType),
			Quantity:     order.OrderQuantity,
			OrderType:    k.orderTypeTo(order.ExecutionType),
			ExpireDay:    timestamppb.New(k.clock.Now()),
		})
	} else {
		res, err = k.kabucom.SendMarginOrder(context.Background(), &kabuspb.SendMarginOrderRequest{
			Password:        strategy.Account.Password,
			SymbolCode:      strategy.SymbolCode,
			Exchange:        kabuspb.StockExchange(k.exchangeTo(order.Exchange)),
			Side:            k.sideTo(order.Side),
			TradeType:       k.tradeTypeTo(order.TradeType),
			MarginTradeType: k.marginTradeTypeTo(order.StockMarginTradeType),
			DeliveryType:    kabuspb.DeliveryType_DELIVERY_TYPE_CASH, // お預かり金 多分固定で大丈夫
			AccountType:     k.accountTypeTo(strategy.Account.AccountType),
			Quantity:        order.OrderQuantity,
			ClosePositions:  k.closePositionsTo(order.ExitPositions),
			OrderType:       k.orderTypeTo(order.ExecutionType),
			Price:           order.Price,
			ExpireDay:       timestamppb.New(k.clock.Now()),
		})
	}
	if err != nil {
		return nil, err
	}

	return &OrderResult{
		Result:        res.ResultCode == 0,
		Code:          res.OrderId,
		ErrorCode:     strconv.Itoa(int(res.ResultCode)),
		OrderDateTime: k.clock.Now(),
	}, nil
}

// RegisterSymbol - 銘柄登録
func (k *kabusAPI) RegisterSymbol(symbolCode string, exchange Exchange) error {
	_, err := k.kabucom.RegisterSymbols(context.Background(), &kabuspb.RegisterSymbolsRequest{
		RequesterName: "grid-rebalance",
		Symbols:       []*kabuspb.RegisterSymbol{{SymbolCode: symbolCode, Exchange: k.exchangeTo(exchange)}},
	})
	return err
}

// GetSymbol - 銘柄情報の取得
func (k *kabusAPI) GetSymbol(symbolCode string, exchange Exchange) (*Symbol, error) {
	symbol, err := k.kabucom.GetSymbol(context.Background(), &kabuspb.GetSymbolRequest{SymbolCode: symbolCode, Exchange: k.exchangeTo(exchange)})
	if err != nil {
		return nil, err
	}
	board, err := k.kabucom.GetBoard(context.Background(), &kabuspb.GetBoardRequest{SymbolCode: symbolCode, Exchange: k.exchangeTo(exchange)})
	if err != nil {
		return nil, err
	}
	return &Symbol{
		Code:             symbol.Code,
		Exchange:         k.exchangeFrom(symbol.Exchange),
		TradingUnit:      symbol.TradingUnit,
		UpperLimit:       symbol.UpperLimit,
		LowerLimit:       symbol.LowerLimit,
		CurrentPrice:     board.CurrentPrice,
		CalculationPrice: board.CalculationPrice,
		BidPrice:         board.BidPrice,
		AskPrice:         board.AskPrice,
	}, nil
}
