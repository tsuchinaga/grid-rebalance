package grid_rebalance

import (
	"errors"
	"fmt"
	"log"

	gerrors "github.com/genjidb/genji/errors"

	"github.com/genjidb/genji"
)

var store *genji.DB

func init() {
	if store == nil {
		s, err := newStore("grid_rebalance.db")
		if err != nil {
			log.Fatalln(err)
		}
		store = s
	}
}

func newStore(path string) (*genji.DB, error) {
	db, err := genji.Open(path)
	if err != nil {
		return nil, err
	}

	// 必須テーブルの作成
	sqlList := []string{
		// strategies
		`create table if not exists strategies`,
		`create unique index if not exists strategies_code on strategies (code)`,
		// orders
		`create table if not exists orders`,
		`create unique index if not exists orders_code on orders (code)`,
		`create index if not exists orders_strategy_code on orders (strategycode)`,
		`create index if not exists orders_status on orders (status)`,
		// positions
		`create table if not exists positions`,
		`create unique index if not exists positions_code on positions (code)`,
		`create index if not exists positions_strategy_code_symbol_code on positions (strategycode, symbolcode)`,
	}

	for _, sql := range sqlList {
		if err := db.Exec(sql); err != nil {
			return nil, fmt.Errorf("error sql: `%s`: %w", sql, err)
		}
	}

	return db, nil
}

type baseStore struct{}

func (s *baseStore) WrapErr(err error) error {
	switch {
	case errors.Is(gerrors.ErrDocumentNotFound, err):
		return fmt.Errorf("genji error: %s: %w", err, ErrDataNotFound)
	case errors.Is(gerrors.ErrDuplicateDocument, err):
		return fmt.Errorf("genji error: %s: %w", err, ErrDataDuplicated)
	case errors.Is(gerrors.AlreadyExistsError{}, err):
		return fmt.Errorf("genji error: %s: %w", err, ErrAlreadyExists)
	case errors.Is(gerrors.NotFoundError{}, err):
		return fmt.Errorf("genji error: %s: %w", err, ErrNotFound)
	default:
		return err
	}
}
