package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"
)

type testStrategyService struct {
	IStrategyService
	FindActiveStrategies1 []*Strategy
	FindActiveStrategies2 error
	Register1             error
	Remove1               error
	ConvertStrategy1      *Strategy
}

func (t *testStrategyService) ConvertStrategy(RegisterStrategy) *Strategy {
	return t.ConvertStrategy1
}

func (t *testStrategyService) FindActiveStrategies() ([]*Strategy, error) {
	return t.FindActiveStrategies1, t.FindActiveStrategies2
}

func (t *testStrategyService) Register(*Strategy) error {
	return t.Register1
}

func (t *testStrategyService) Remove(string) error {
	return t.Remove1
}

func Test_strategyService_Register(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name              string
		strategyStore     *testStrategyStore
		kabusAPI          *testKabusAPI
		arg               *Strategy
		want1             error
		wantInsertHistory []interface{}
	}{
		{name: "引数がnilならエラー",
			strategyStore: &testStrategyStore{},
			arg:           nil,
			want1:         ErrNilArguments},
		{name: "codeが設定されていなければエラー",
			strategyStore: &testStrategyStore{},
			arg:           &Strategy{Code: ""},
			want1:         ErrStrategyValidation},
		{name: "codeが重複していたらエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: true},
			arg:           &Strategy{Code: "strategy-1475"},
			want1:         ErrStrategyValidation},
		{name: "apiで銘柄が取れなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol2: ErrUnknown},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou},
			want1:         ErrUnknown},
		{name: "現物・信用区分が設定されていなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeUnspecified},
			want1:         ErrStrategyValidation},
		{name: "エントリー方向が指定されていなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeMarginDay, EntrySide: SideUnspecified},
			want1:         ErrStrategyValidation},
		{name: "現物でエントリー方向が買いでなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideSell},
			want1:         ErrStrategyValidation},
		{name: "グリッド幅が1未満だとエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 0},
			want1:         ErrStrategyValidation},
		{name: "グリッド数が1未満だとエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 0},
			want1:         ErrStrategyValidation},
		{name: "注文数量が1未満だとエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 0},
			want1:         ErrStrategyValidation},
		{name: "割当資産額が0以下だとエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 0},
			want1:         ErrStrategyValidation},
		{name: "注文パスワードが設定されていなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: ""}},
			want1:         ErrStrategyValidation},
		{name: "口座種別が設定されていなければエラー",
			strategyStore: &testStrategyStore{IsExistsByCode1: false},
			kabusAPI:      &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:           &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: "Password1234", AccountType: AccountTypeUnspecified}},
			want1:         ErrStrategyValidation},
		{name: "insertでエラーが出たらエラー",
			strategyStore:     &testStrategyStore{IsExistsByCode1: false, Insert1: ErrUnknown},
			kabusAPI:          &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:               &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
			want1:             ErrUnknown,
			wantInsertHistory: []interface{}{&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}, TradingUnit: 1}}},
		{name: "insertでエラーがなければnilが返される",
			strategyStore:     &testStrategyStore{IsExistsByCode1: false, Insert1: nil},
			kabusAPI:          &testKabusAPI{GetSymbol1: &Symbol{TradingUnit: 1}},
			arg:               &Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}},
			want1:             nil,
			wantInsertHistory: []interface{}{&Strategy{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou, StockMarginTradeType: StockMarginTradeTypeStock, EntrySide: SideBuy, GridWidth: 1, GridNum: 1, Quantity: 1, AllottedAsset: 100_000, Account: Account{Password: "Password1234", AccountType: AccountTypeSpecific}, TradingUnit: 1}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &strategyService{strategyStore: test.strategyStore, kabusAPI: test.kabusAPI}
			got1 := service.Register(test.arg)
			if !errors.Is(got1, test.want1) || !reflect.DeepEqual(test.wantInsertHistory, test.strategyStore.InsertHistory) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.wantInsertHistory, got1, test.strategyStore.InsertHistory)
			}
		})
	}
}

func Test_strategyService_FindActiveStrategies(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		strategyStore *testStrategyStore
		want1         []*Strategy
		want2         error
	}{
		{name: "errが返されたらerrを返す",
			strategyStore: &testStrategyStore{FindActiveStrategies1: nil, FindActiveStrategies2: ErrUnknown},
			want1:         nil,
			want2:         ErrUnknown},
		{name: "errなく構造体が返されたら構造体を返す",
			strategyStore: &testStrategyStore{FindActiveStrategies1: []*Strategy{{Code: "strategy-1475"}}, FindActiveStrategies2: nil},
			want1:         []*Strategy{{Code: "strategy-1475"}},
			want2:         nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &strategyService{strategyStore: test.strategyStore}
			got1, got2 := service.FindActiveStrategies()
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_strategyService_Remove(t *testing.T) {
	t.Parallel()
	tests := []struct {
		strategyStore   *testStrategyStore
		name            string
		arg1            string
		want1           error
		wantDeleteCount int
	}{
		{name: "指定したcodeのデータがなければ何もしない",
			strategyStore:   &testStrategyStore{IsExistsByCode1: false},
			arg1:            "strategy-code",
			want1:           nil,
			wantDeleteCount: 0},
		{name: "deleteで失敗したらerrを返す",
			strategyStore:   &testStrategyStore{IsExistsByCode1: true, Delete1: ErrUnknown},
			arg1:            "strategy-code",
			want1:           ErrUnknown,
			wantDeleteCount: 1},
		{name: "deleteでエラーがなければnilを返す",
			strategyStore:   &testStrategyStore{IsExistsByCode1: true, Delete1: nil},
			arg1:            "strategy-code",
			want1:           nil,
			wantDeleteCount: 1},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &strategyService{strategyStore: test.strategyStore}
			got1 := service.Remove(test.arg1)
			if !errors.Is(got1, test.want1) || !reflect.DeepEqual(test.wantDeleteCount, test.strategyStore.DeleteCount) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.wantDeleteCount, got1, test.strategyStore.DeleteCount)
			}
		})
	}
}

func Test_strategyService_ConvertStrategy(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		arg1  RegisterStrategy
		want1 *Strategy
	}{
		{name: "各項目を変換して登録できる",
			arg1: RegisterStrategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeStock,
				EntrySide:            SideBuy,
				GridWidth:            2,
				GridNum:              8,
				Quantity:             4,
				AllottedAsset:        500_000,
				OrderPassword:        "Password1234",
				AccountType:          AccountTypeSpecific,
			},
			want1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeStock,
				EntrySide:            SideBuy,
				GridWidth:            2,
				GridNum:              8,
				Quantity:             4,
				AllottedAsset:        500_000,
				ManagementAsset:      500_000,
				Cash:                 500_000,
				TradingUnit:          0,
				Active:               true,
				Account: Account{
					Password:    "Password1234",
					AccountType: AccountTypeSpecific,
				},
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &strategyService{}
			got1 := service.ConvertStrategy(test.arg1)
			if !reflect.DeepEqual(test.want1, got1) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want1, got1)
			}
		})
	}
}
