package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"
	"time"
)

type testPositionService struct {
	IPositionService
	TotalQuantityByStrategyCodeAndSymbolCodeAndSide1 float64
	TotalQuantityByStrategyCodeAndSymbolCodeAndSide2 error
	GetAndHoldQuantity1                              []HoldPosition
	GetAndHoldQuantity2                              error
	GetAndHoldQuantityHistory                        []interface{}
	ReleaseHoldQuantity1                             error
	ReleaseHoldQuantityHistory                       []interface{}
}

func (t *testPositionService) TotalQuantityByStrategyCodeAndSymbolCodeAndSide(string, string, Side) (float64, error) {
	return t.TotalQuantityByStrategyCodeAndSymbolCodeAndSide1, t.TotalQuantityByStrategyCodeAndSymbolCodeAndSide2
}

func (t *testPositionService) GetAndHoldQuantity(strategyCode string, symbolCode string, side Side, quantity float64, holdOrder HoldOrder) ([]HoldPosition, error) {
	if t.GetAndHoldQuantityHistory == nil {
		t.GetAndHoldQuantityHistory = make([]interface{}, 0)
	}
	t.GetAndHoldQuantityHistory = append(t.GetAndHoldQuantityHistory, []interface{}{strategyCode, symbolCode, side, quantity, holdOrder}...)
	return t.GetAndHoldQuantity1, t.GetAndHoldQuantity2
}

func (t *testPositionService) ReleaseHoldQuantity(holdPositions []HoldPosition) error {
	if t.ReleaseHoldQuantityHistory == nil {
		t.ReleaseHoldQuantityHistory = make([]interface{}, 0)
	}
	t.ReleaseHoldQuantityHistory = append(t.ReleaseHoldQuantityHistory, holdPositions)
	return t.ReleaseHoldQuantity1
}

func Test_positionService_TotalQuantityByStrategyCodeAndSymbolCodeAndSide(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		positionStore *testPositionStore
		arg1          string
		arg2          string
		arg3          Side
		want1         float64
		want2         error
	}{
		{name: "storeがerrを返したらerrを返す",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide2: ErrUnknown},
			arg1:          "strategy-1475",
			arg2:          "1475",
			arg3:          SideBuy,
			want1:         0,
			want2:         ErrUnknown},
		{name: "storeが返したpositionの保有数を加算して返す",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide1: []*Position{
				{OwnedQuantity: 100},
				{OwnedQuantity: 50},
				{OwnedQuantity: 30},
			}},
			arg1:  "strategy-1475",
			arg2:  "1475",
			arg3:  SideBuy,
			want1: 180,
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &positionService{positionStore: test.positionStore}
			got1, got2 := service.TotalQuantityByStrategyCodeAndSymbolCodeAndSide(test.arg1, test.arg2, test.arg3)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_positionService_GetAndHoldQuantity(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name                  string
		positionStore         *testPositionStore
		arg1                  string
		arg2                  string
		arg3                  Side
		arg4                  float64
		arg5                  HoldOrder
		want1                 []HoldPosition
		want2                 error
		wantUpdateManyHistory []interface{}
	}{
		{name: "position取得でerrorがあればerrを返す",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide2: ErrUnknown},
			want1:         nil,
			want2:         ErrUnknown},
		{name: "position全部で必要数が足りなかったらエラーを返す",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide1: []*Position{
				{OwnedQuantity: 30},
				{OwnedQuantity: 50},
			}},
			arg1:  "strategy-1475",
			arg2:  "1475",
			arg3:  SideBuy,
			arg4:  100,
			arg5:  HoldOrderFirst,
			want1: nil,
			want2: ErrNotEnoughOwnedPositions},
		{name: "updateでエラーが出たら途中で処理を止める",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide1: []*Position{
				{Code: "code0", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 0, 0, 0, time.Local)},
				{Code: "code1", OwnedQuantity: 50, ContractDateTime: time.Date(2021, 10, 15, 14, 1, 0, 0, time.Local)},
				{Code: "code2", OwnedQuantity: 80, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				{Code: "code3", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 3, 0, 0, time.Local)},
			}},
			arg1: "strategy-1475",
			arg2: "1475",
			arg3: SideBuy,
			arg4: 100,
			arg5: HoldOrderFirst,
			want1: []HoldPosition{
				{PositionCode: "code0", HoldQuantity: 30},
				{PositionCode: "code1", HoldQuantity: 50},
				{PositionCode: "code2", HoldQuantity: 20},
			},
			wantUpdateManyHistory: []interface{}{
				[]*Position{
					{Code: "code0", OwnedQuantity: 30, HoldQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 0, 0, 0, time.Local)},
					{Code: "code1", OwnedQuantity: 50, HoldQuantity: 50, ContractDateTime: time.Date(2021, 10, 15, 14, 1, 0, 0, time.Local)},
					{Code: "code2", OwnedQuantity: 80, HoldQuantity: 20, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				}}},
		{name: "古い順で拘束し、updateできる",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide1: []*Position{
				{Code: "code0", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 0, 0, 0, time.Local)},
				{Code: "code1", OwnedQuantity: 50, ContractDateTime: time.Date(2021, 10, 15, 14, 1, 0, 0, time.Local)},
				{Code: "code2", OwnedQuantity: 80, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				{Code: "code3", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 3, 0, 0, time.Local)},
			}},
			arg1: "strategy-1475",
			arg2: "1475",
			arg3: SideBuy,
			arg4: 100,
			arg5: HoldOrderFirst,
			want1: []HoldPosition{
				{PositionCode: "code0", HoldQuantity: 30},
				{PositionCode: "code1", HoldQuantity: 50},
				{PositionCode: "code2", HoldQuantity: 20},
			},
			wantUpdateManyHistory: []interface{}{
				[]*Position{
					{Code: "code0", OwnedQuantity: 30, HoldQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 0, 0, 0, time.Local)},
					{Code: "code1", OwnedQuantity: 50, HoldQuantity: 50, ContractDateTime: time.Date(2021, 10, 15, 14, 1, 0, 0, time.Local)},
					{Code: "code2", OwnedQuantity: 80, HoldQuantity: 20, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				}}},
		{name: "新しい順で拘束し、updateできる",
			positionStore: &testPositionStore{FindByStrategyCodeAndSymbolCodeAndSide1: []*Position{
				{Code: "code0", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 0, 0, 0, time.Local)},
				{Code: "code1", OwnedQuantity: 50, ContractDateTime: time.Date(2021, 10, 15, 14, 1, 0, 0, time.Local)},
				{Code: "code2", OwnedQuantity: 80, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				{Code: "code3", OwnedQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 3, 0, 0, time.Local)},
			}},
			arg1: "strategy-1475",
			arg2: "1475",
			arg3: SideBuy,
			arg4: 100,
			arg5: HoldOrderLatest,
			want1: []HoldPosition{
				{PositionCode: "code3", HoldQuantity: 30},
				{PositionCode: "code2", HoldQuantity: 70},
			},
			wantUpdateManyHistory: []interface{}{
				[]*Position{
					{Code: "code3", OwnedQuantity: 30, HoldQuantity: 30, ContractDateTime: time.Date(2021, 10, 15, 14, 3, 0, 0, time.Local)},
					{Code: "code2", OwnedQuantity: 80, HoldQuantity: 70, ContractDateTime: time.Date(2021, 10, 15, 14, 2, 0, 0, time.Local)},
				}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &positionService{positionStore: test.positionStore}
			got1, got2 := service.GetAndHoldQuantity(test.arg1, test.arg2, test.arg3, test.arg4, test.arg5)
			if !reflect.DeepEqual(test.want1, got1) ||
				!errors.Is(got2, test.want2) ||
				!reflect.DeepEqual(test.wantUpdateManyHistory, test.positionStore.UpdateManyHistory) {
				t.Errorf("%s error\nwant: %+v, %+v, %+v\ngot: %+v, %+v, %+v\n", t.Name(),
					test.want1, test.want2, test.wantUpdateManyHistory,
					got1, got2, test.positionStore.UpdateManyHistory)
			}
		})
	}
}

func Test_positionService_ReleaseHoldQuantity(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name                  string
		positionStore         *testPositionStore
		arg                   []HoldPosition
		want                  error
		wantUpdateManyHistory []interface{}
	}{
		{name: "引数がnilなら何もしない",
			positionStore: &testPositionStore{},
			arg:           nil,
			want:          nil},
		{name: "引数が空配列なら何もしない",
			positionStore: &testPositionStore{},
			arg:           []HoldPosition{},
			want:          nil},
		{name: "保存済みのポジション取得でエラーが出たらエラー",
			positionStore: &testPositionStore{FindByCode2: ErrUnknown},
			arg:           []HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 10}},
			want:          ErrUnknown},
		{name: "更新すべきポジションがあればupdateを通す",
			positionStore:         &testPositionStore{FindByCode1: &Position{Code: "position-code-1", HoldQuantity: 30}},
			arg:                   []HoldPosition{{PositionCode: "position-code-1", HoldQuantity: 10}},
			want:                  nil,
			wantUpdateManyHistory: []interface{}{[]*Position{{Code: "position-code-1", HoldQuantity: 20}}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &positionService{positionStore: test.positionStore}
			got := service.ReleaseHoldQuantity(test.arg)
			if !errors.Is(got, test.want) || !reflect.DeepEqual(test.wantUpdateManyHistory, test.positionStore.UpdateManyHistory) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(),
					test.want, test.wantUpdateManyHistory,
					got, test.positionStore.UpdateManyHistory)
			}
		})
	}
}
