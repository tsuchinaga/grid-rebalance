package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"

	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

type testStrategyStore struct {
	IStrategyStore
	FindByCode1           *Strategy
	FindByCode2           error
	FindActiveStrategies1 []*Strategy
	FindActiveStrategies2 error
	IsExistsByCode1       bool
	Insert1               error
	InsertHistory         []interface{}
	Delete1               error
	DeleteHistory         []interface{}
	DeleteCount           int
}

func (t *testStrategyStore) FindByCode(string) (*Strategy, error) {
	return t.FindByCode1, t.FindByCode2
}

func (t *testStrategyStore) FindActiveStrategies() ([]*Strategy, error) {
	return t.FindActiveStrategies1, t.FindActiveStrategies2
}

func (t *testStrategyStore) IsExistsByCode(string) bool {
	return t.IsExistsByCode1
}

func (t *testStrategyStore) Insert(strategy *Strategy) error {
	if t.InsertHistory == nil {
		t.InsertHistory = make([]interface{}, 0)
	}
	t.InsertHistory = append(t.InsertHistory, strategy)
	return t.Insert1
}

func (t *testStrategyStore) Delete(code string) error {
	if t.DeleteHistory == nil {
		t.DeleteHistory = make([]interface{}, 0)
	}
	t.DeleteHistory = append(t.DeleteHistory, code)
	t.DeleteCount++
	return t.Delete1
}

func Test_strategyStore_FindByCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Strategy
		arg1    string
		want1   *Strategy
		want2   error
	}{
		{name: "codeのデータがなければerror",
			dataset: []*Strategy{
				{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: true},
			},
			arg1:  "no-code",
			want1: nil,
			want2: ErrDataNotFound},
		{name: "codeのデータがあればstrategyに詰めて返す",
			dataset: []*Strategy{
				{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: true},
			},
			arg1:  "1475",
			want1: &Strategy{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into strategies values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &strategyStore{store: db}
			got1, got2 := store.FindByCode(test.arg1)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_strategyStore_FindActiveStrategies(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Strategy
		want1   []*Strategy
		want2   error
	}{
		{name: "有効な戦略がなければ空スライスを返す",
			dataset: []*Strategy{
				{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: false},
			},
			want1: []*Strategy{},
			want2: nil},
		{name: "有効な戦略があればstrategyに詰めてスライスに入れて返す",
			dataset: []*Strategy{
				{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: true},
			},
			want1: []*Strategy{
				{Code: "1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: true},
				{Code: "1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: true},
			},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into strategies values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &strategyStore{store: db}
			got1, got2 := store.FindActiveStrategies()
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_strategyStore_IsExistsByCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Strategy
		arg     string
		want1   bool
	}{
		{name: "該当するコードがなければfalse",
			dataset: []*Strategy{
				{Code: "strategy-1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "strategy-1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "strategy-1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: false},
			},
			arg:   "strategy-0000",
			want1: false},
		{name: "該当するコードがあればtrue",
			dataset: []*Strategy{
				{Code: "strategy-1475", SymbolCode: "1475", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "strategy-1476", SymbolCode: "1476", GridWidth: 1, GridNum: 5, Quantity: 2, ManagementAsset: 500_000, Active: false},
				{Code: "strategy-1699", SymbolCode: "1699", GridWidth: 1, GridNum: 5, Quantity: 10, ManagementAsset: 500_000, Active: false},
			},
			arg:   "strategy-1475",
			want1: true},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into strategies values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &strategyStore{store: db}
			got1 := store.IsExistsByCode(test.arg)
			if !reflect.DeepEqual(test.want1, got1) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want1, got1)
			}
		})
	}
}

func Test_strategyStore_Insert(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name           string
		dataset        []*Strategy
		arg            *Strategy
		want           error
		wantStrategies []*Strategy
	}{
		{name: "insertしたデータが取れる",
			dataset:        []*Strategy{},
			arg:            &Strategy{Code: "strategy-1234"},
			want:           nil,
			wantStrategies: []*Strategy{{Code: "strategy-1234"}}},
		{name: "uniqueキー制約に引っかかったらinsert出来ない",
			dataset: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-2345"},
				{Code: "strategy-3456"},
			},
			arg:  &Strategy{Code: "strategy-1234"},
			want: ErrDataDuplicated,
			wantStrategies: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-2345"},
				{Code: "strategy-3456"},
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into strategies values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &strategyStore{store: db}
			got := store.Insert(test.arg)

			strategies := make([]*Strategy, 0)
			res, _ := db.Query("select * from strategies")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var strategy Strategy
				_ = document.StructScan(d, &strategy)
				strategies = append(strategies, &strategy)
				return nil
			})

			if !reflect.DeepEqual(test.wantStrategies, strategies) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_strategyStore_Delete(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name           string
		dataset        []*Strategy
		arg            string
		want           error
		wantStrategies []*Strategy
	}{
		{name: "deleteしたデータが消える",
			dataset: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-2345"},
				{Code: "strategy-3456"},
			},
			arg:  "strategy-2345",
			want: nil,
			wantStrategies: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-3456"},
			}},
		{name: "指定したコードが存在しなければ何も消えない",
			dataset: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-2345"},
				{Code: "strategy-3456"},
			},
			arg:  "strategy-0000",
			want: nil,
			wantStrategies: []*Strategy{
				{Code: "strategy-1234"},
				{Code: "strategy-2345"},
				{Code: "strategy-3456"},
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into strategies values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &strategyStore{store: db}
			got := store.Delete(test.arg)

			strategies := make([]*Strategy, 0)
			res, _ := db.Query("select * from strategies")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var strategy Strategy
				_ = document.StructScan(d, &strategy)
				strategies = append(strategies, &strategy)
				return nil
			})

			if !reflect.DeepEqual(test.wantStrategies, strategies) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}
