package grid_rebalance

import (
	"fmt"
	"io"
	"log"
	"os"
)

// ILogger - ロガーのインターフェース
type ILogger interface {
	Stdout(v ...interface{})
	Notice(v ...interface{})
	Warning(v ...interface{})
}

// logger - ロガー
type logger struct {
	stdout  *log.Logger
	notice  *log.Logger
	warning *log.Logger
}

func (l *logger) Stdout(v ...interface{}) {
	if l.stdout == nil {
		l.notice = log.New(os.Stdout, "", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	}

	_ = l.stdout.Output(2, fmt.Sprintln(v...))
}

func (l *logger) Notice(v ...interface{}) {
	if l.notice == nil {
		notice, err := os.OpenFile("notice.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		l.notice = log.New(io.MultiWriter(os.Stdout, notice), "", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	}

	_ = l.notice.Output(2, fmt.Sprintln(v...))
}

func (l *logger) Warning(v ...interface{}) {
	if l.warning == nil {
		warning, err := os.OpenFile("warning.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		l.warning = log.New(io.MultiWriter(os.Stderr, warning), "", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	}

	_ = l.warning.Output(2, fmt.Sprintln(v...))
}
