package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"

	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

type testPositionStore struct {
	IPositionStore
	FindByCode1                             *Position
	FindByCode2                             error
	FindByStrategyCodeAndSymbolCode1        []*Position
	FindByStrategyCodeAndSymbolCode2        error
	FindByStrategyCodeAndSymbolCodeAndSide1 []*Position
	FindByStrategyCodeAndSymbolCodeAndSide2 error
	UpdateMany1                             error
	UpdateManyHistory                       []interface{}
}

func (t *testPositionStore) FindByCode(string) (*Position, error) {
	return t.FindByCode1, t.FindByCode2
}

func (t *testPositionStore) FindByStrategyCodeAndSymbolCode(string, string) ([]*Position, error) {
	return t.FindByStrategyCodeAndSymbolCode1, t.FindByStrategyCodeAndSymbolCode2
}

func (t *testPositionStore) FindByStrategyCodeAndSymbolCodeAndSide(string, string, Side) ([]*Position, error) {
	return t.FindByStrategyCodeAndSymbolCodeAndSide1, t.FindByStrategyCodeAndSymbolCodeAndSide2
}

func (t *testPositionStore) UpdateMany(positions []*Position) error {
	if t.UpdateManyHistory == nil {
		t.UpdateManyHistory = make([]interface{}, 0)
	}
	t.UpdateManyHistory = append(t.UpdateManyHistory, positions)
	return t.UpdateMany1
}

func Test_positionStore_FindByStrategyCodeAndSymbolCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Position
		arg1    string
		arg2    string
		want1   []*Position
		want2   error
	}{
		{name: "該当するデータがなければ空",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", OwnedQuantity: 120},
			},
			arg1:  "strategy-code",
			arg2:  "0000",
			want1: []*Position{},
			want2: nil},
		{name: "codeのデータがあればsliceに詰めて返す",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", OwnedQuantity: 120},
			},
			arg1:  "strategy-1476",
			arg2:  "1476",
			want1: []*Position{{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", OwnedQuantity: 70}},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got1, got2 := store.FindByStrategyCodeAndSymbolCode(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_positionStore_FindByStrategyCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Position
		arg1    string
		want1   []*Position
		want2   error
	}{
		{name: "対象のデータがなければなにもしない",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", OwnedQuantity: 120},
			},
			arg1:  "strategy-0000",
			want1: []*Position{},
			want2: nil},
		{name: "対象のデータがあればpositionのsliceに詰めて返す",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", OwnedQuantity: 120},
			},
			arg1: "strategy-1699",
			want1: []*Position{
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", OwnedQuantity: 120},
			},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got1, got2 := store.FindByStrategyCode(test.arg1)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_positionStore_Insert(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		dataset       []*Position
		arg           *Position
		want          error
		wantPositions []*Position
	}{
		{name: "insertしたデータが取れる",
			dataset:       []*Position{},
			arg:           &Position{StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			want:          nil,
			wantPositions: []*Position{{StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}}},
		{name: "uniqueキー制約に引っかかったらinsert出来ない",
			dataset:       []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg:           &Position{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			want:          ErrDataDuplicated,
			wantPositions: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got := store.Insert(test.arg)

			positions := make([]*Position, 0)
			res, _ := db.Query("select * from positions")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var position Position
				_ = document.StructScan(d, &position)
				positions = append(positions, &position)
				return nil
			})

			if !reflect.DeepEqual(test.wantPositions, positions) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_positionStore_FindByStrategyCodeAndSymbolCodeAndSide(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Position
		arg1    string
		arg2    string
		arg3    Side
		want1   []*Position
		want2   error
	}{
		{name: "該当するデータがなければ空",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideBuy, OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", Side: SideBuy, OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", Side: SideBuy, OwnedQuantity: 120},
			},
			arg1:  "strategy-1476",
			arg2:  "1476",
			arg3:  SideSell,
			want1: []*Position{},
			want2: nil},
		{name: "codeのデータがあればsliceに詰めて返す",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideBuy, OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1476", SymbolCode: "1476", Side: SideBuy, OwnedQuantity: 70},
				{Code: "code-0003", StrategyCode: "strategy-1699", SymbolCode: "1699", Side: SideBuy, OwnedQuantity: 120},
				{Code: "code-0004", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideBuy, OwnedQuantity: 150},
				{Code: "code-0005", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideSell, OwnedQuantity: 50},
			},
			arg1: "strategy-1475",
			arg2: "1475",
			arg3: SideBuy,
			want1: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideBuy, OwnedQuantity: 100},
				{Code: "code-0004", StrategyCode: "strategy-1475", SymbolCode: "1475", Side: SideBuy, OwnedQuantity: 150},
			},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got1, got2 := store.FindByStrategyCodeAndSymbolCodeAndSide(test.arg1, test.arg2, test.arg3)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_positionStore_Update(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		dataset       []*Position
		arg1          *Position
		want          error
		wantPositions []*Position
	}{
		{name: "引数nilはエラー",
			dataset:       []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:          nil,
			want:          ErrNilArguments,
			wantPositions: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}}},
		{name: "更新前後で中身が一致していたら変化なし",
			dataset:       []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:          &Position{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			want:          nil,
			wantPositions: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}}},
		{name: "更新前に一致するcodeのデータがなければ追加される",
			dataset: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:    &Position{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			want:    nil,
			wantPositions: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			}},
		{name: "codeに一致するデータがあれば置き換える",
			dataset:       []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:          &Position{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 200, HoldQuantity: 100},
			want:          nil,
			wantPositions: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 200, HoldQuantity: 100}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got := store.Update(test.arg1)

			positions := make([]*Position, 0)
			res, _ := db.Query("select * from positions")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var position Position
				_ = document.StructScan(d, &position)
				positions = append(positions, &position)
				return nil
			})

			if !reflect.DeepEqual(test.wantPositions, positions) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want, test.wantPositions, got, positions)
			}
		})
	}
}

func Test_positionStore_UpdateMany(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		dataset       []*Position
		arg1          []*Position
		want          error
		wantPositions []*Position
	}{
		{name: "更新前後で中身が一致していたら変化なし",
			dataset:       []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:          []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			want:          nil,
			wantPositions: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}}},
		{name: "更新前に一致するcodeのデータがなければ追加される",
			dataset: []*Position{{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			arg1:    []*Position{{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100}},
			want:    nil,
			wantPositions: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
			}},
		{name: "codeに一致するデータがあれば置き換える",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50},
			},
			arg1: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 200, HoldQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50, HoldQuantity: 50},
			},
			want: nil,
			wantPositions: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 200, HoldQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50, HoldQuantity: 50},
			}},
		{name: "一部のデータでエラーが出たら全部未反映に戻す",
			dataset: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50},
			},
			arg1: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100, HoldQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50, HoldQuantity: 50},
				nil,
			},
			want: ErrNilArguments,
			wantPositions: []*Position{
				{Code: "code-0001", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 100},
				{Code: "code-0002", StrategyCode: "strategy-1475", SymbolCode: "1475", OwnedQuantity: 50},
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got := store.UpdateMany(test.arg1)

			positions := make([]*Position, 0)
			res, _ := db.Query("select * from positions")
			defer res.Close()
			_ = res.Iterate(func(d types.Document) error {
				var position Position
				_ = document.StructScan(d, &position)
				positions = append(positions, &position)
				return nil
			})

			if !reflect.DeepEqual(test.wantPositions, positions) || !errors.Is(got, test.want) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want, test.wantPositions, got, positions)
			}
		})
	}
}

func Test_positionStore_FindByCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name    string
		dataset []*Position
		arg1    string
		want1   *Position
		want2   error
	}{
		{name: "codeのデータがなければerror",
			dataset: []*Position{
				{Code: "position-0010", StrategyCode: "1475", SymbolCode: "1475"},
				{Code: "position-0011", StrategyCode: "1475", SymbolCode: "1475"},
				{Code: "position-0012", StrategyCode: "1475", SymbolCode: "1475"},
			},
			arg1:  "no-code",
			want1: nil,
			want2: ErrDataNotFound},
		{name: "codeのデータがあればpositionに詰めて返す",
			dataset: []*Position{
				{Code: "position-0010", StrategyCode: "1475", SymbolCode: "1475"},
				{Code: "position-0011", StrategyCode: "1475", SymbolCode: "1475"},
				{Code: "position-0012", StrategyCode: "1475", SymbolCode: "1475"},
			},
			arg1:  "position-0011",
			want1: &Position{Code: "position-0011", StrategyCode: "1475", SymbolCode: "1475"},
			want2: nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			db, _ := newStore(":memory:")
			defer db.Close()
			for _, data := range test.dataset {
				if err := db.Exec(`insert into positions values ?`, data); err != nil {
					t.Errorf("%s insert error\n%+v\n", t.Name(), err)
				}
			}

			store := &positionStore{store: db}
			got1, got2 := store.FindByCode(test.arg1)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}
