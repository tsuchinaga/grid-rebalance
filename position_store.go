package grid_rebalance

import (
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

// IPositionStore - ポジションストアのインターフェース
type IPositionStore interface {
	FindByCode(code string) (*Position, error)
	FindByStrategyCode(strategyCode string) ([]*Position, error)
	FindByStrategyCodeAndSymbolCode(strategyCode string, symbolCode string) ([]*Position, error)
	FindByStrategyCodeAndSymbolCodeAndSide(strategyCode string, symbolCode string, side Side) ([]*Position, error)
	Insert(position *Position) error
	Update(position *Position) error
	UpdateMany(positions []*Position) error
}

// positionStore - ポジションストア
type positionStore struct {
	baseStore
	store *genji.DB
}

// documentToPosition - 結果のドキュメントをPosition構造体に詰める
func (s *positionStore) documentToPosition(doc types.Document) (*Position, error) {
	var position Position
	if err := document.StructScan(doc, &position); err != nil {
		return nil, s.WrapErr(err)
	}
	return &position, nil
}

// iterateToPositionSlice - 結果をPositionスライスに詰める
func (s *positionStore) iterateToPositionSlice(res *genji.Result) ([]*Position, error) {
	result := make([]*Position, 0)
	err := res.Iterate(func(d types.Document) error {
		var position Position
		if err := document.StructScan(d, &position); err != nil {
			return err
		}
		result = append(result, &position)
		return nil
	})
	if err != nil {
		return nil, s.WrapErr(err)
	}
	return result, nil
}

// FindByCode - codeでpositionを取得する
func (s *positionStore) FindByCode(code string) (*Position, error) {
	doc, err := s.store.QueryDocument(`select * from positions where code = ?`, code)
	if err != nil {
		return nil, s.WrapErr(err)
	}

	return s.documentToPosition(doc)
}

// FindByStrategyCodeAndSymbolCode - 戦略と銘柄でポジションを取り出す
func (s *positionStore) FindByStrategyCodeAndSymbolCode(strategyCode string, symbolCode string) ([]*Position, error) {
	res, err := s.store.Query(`select * from positions where strategycode = ? and symbolcode = ?`, strategyCode, symbolCode)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToPositionSlice(res)
}

func (s *positionStore) FindByStrategyCodeAndSymbolCodeAndSide(strategyCode string, symbolCode string, side Side) ([]*Position, error) {
	res, err := s.store.Query(`select * from positions where strategycode = ? and symbolcode = ? and side = ?`, strategyCode, symbolCode, side)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToPositionSlice(res)
}

// FindByStrategyCode - 戦略に紐づくポジションを取り出す
func (s *positionStore) FindByStrategyCode(strategyCode string) ([]*Position, error) {
	res, err := s.store.Query(`select * from positions where strategycode = ? order by symbolcode`, strategyCode)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToPositionSlice(res)
}

// Insert - データの登録
func (s *positionStore) Insert(order *Position) error {
	return s.WrapErr(s.store.Exec(`insert into positions values ?`, order))
}

func (s *positionStore) update(tx *genji.Tx, position *Position) error {
	// 削除
	if err := tx.Exec(`delete from positions where code = ?`, position.Code); err != nil {
		return s.WrapErr(err)
	}

	// 追加
	if err := tx.Exec(`insert into positions values ?`, position); err != nil {
		return s.WrapErr(err)
	}

	return nil
}

// Update - ドキュメントの更新
func (s *positionStore) Update(position *Position) error {
	if position == nil {
		return ErrNilArguments
	}

	tx, err := s.store.Begin(true)
	if err != nil {
		return s.WrapErr(err)
	}

	if err := s.update(tx, position); err != nil {
		_ = tx.Rollback()
		return s.WrapErr(err)
	}

	_ = tx.Commit()
	return nil
}

// UpdateMany - 複数ドキュメントの更新
func (s *positionStore) UpdateMany(positions []*Position) error {
	tx, err := s.store.Begin(true)
	if err != nil {
		return s.WrapErr(err)
	}

	for _, p := range positions {
		if p == nil {
			_ = tx.Rollback()
			return ErrNilArguments
		}

		if err := s.update(tx, p); err != nil {
			_ = tx.Rollback()
			return s.WrapErr(err)
		}
	}

	_ = tx.Commit()
	return nil
}
