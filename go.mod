module gitlab.com/tsuchinaga/grid-rebalance

go 1.17

require (
	github.com/genjidb/genji v0.13.0
	gitlab.com/tsuchinaga/kabus-grpc-server v0.0.1
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/btree v1.0.1 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.2 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	go.etcd.io/bbolt v1.3.5 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210903162649-d08c68adba83 // indirect
	google.golang.org/grpc v1.40.0 // indirect
)
