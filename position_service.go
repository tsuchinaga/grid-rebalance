package grid_rebalance

import "sort"

// IPositionService - ポジションサービスのインターフェース
type IPositionService interface {
	TotalQuantityByStrategyCodeAndSymbolCodeAndSide(strategyCode string, symbolCode string, side Side) (float64, error)
	GetAndHoldQuantity(strategyCode string, symbolCode string, side Side, quantity float64, holdOrder HoldOrder) ([]HoldPosition, error)
	ReleaseHoldQuantity(holdPositions []HoldPosition) error
}

// positionService - ポジションサービス
type positionService struct {
	positionStore IPositionStore
}

func (s *positionService) TotalQuantityByStrategyCodeAndSymbolCodeAndSide(strategyCode string, symbolCode string, side Side) (float64, error) {
	positions, err := s.positionStore.FindByStrategyCodeAndSymbolCodeAndSide(strategyCode, symbolCode, side)
	if err != nil {
		return 0, err
	}

	var q float64
	for _, p := range positions {
		q += p.OwnedQuantity
	}
	return q, nil
}

// GetAndHoldQuantity - 戦略と銘柄に関連するポジションを拘束し、拘束したポジションを返す
func (s *positionService) GetAndHoldQuantity(strategyCode string, symbolCode string, side Side, quantity float64, holdOrder HoldOrder) ([]HoldPosition, error) {
	positions, err := s.positionStore.FindByStrategyCodeAndSymbolCodeAndSide(strategyCode, symbolCode, side)
	if err != nil {
		return nil, err
	}

	if holdOrder == HoldOrderLatest {
		sort.Slice(positions, func(i, j int) bool {
			return positions[i].ContractDateTime.After(positions[j].ContractDateTime)
		})
	} else {
		sort.Slice(positions, func(i, j int) bool {
			return positions[i].ContractDateTime.Before(positions[j].ContractDateTime)
		})
	}

	var hold float64
	holdPositions := make([]HoldPosition, 0)
	savePositions := make([]*Position, 0)
	for _, p := range positions {
		if quantity == hold {
			break
		}

		q := p.OwnedQuantity - p.HoldQuantity
		if q > quantity-hold {
			q = quantity - hold
		}

		p.HoldQuantity += q
		hold += q
		holdPositions = append(holdPositions, HoldPosition{PositionCode: p.Code, HoldQuantity: q})
		savePositions = append(savePositions, p)
	}

	// 必要数拘束できない場合エラー
	if quantity != hold {
		return nil, ErrNotEnoughOwnedPositions
	}

	// 拘束ポジションの保存
	if err := s.positionStore.UpdateMany(savePositions); err != nil {
		return holdPositions, err
	}

	return holdPositions, nil
}

// ReleaseHoldQuantity - 拘束していたポジションを解放する
func (s *positionService) ReleaseHoldQuantity(holdPositions []HoldPosition) error {
	savePositions := make([]*Position, 0)
	for _, hp := range holdPositions {
		p, err := s.positionStore.FindByCode(hp.PositionCode)
		if err != nil {
			return err
		}
		p.HoldQuantity -= hp.HoldQuantity
		savePositions = append(savePositions, p)
	}

	if len(savePositions) > 0 {
		return s.positionStore.UpdateMany(savePositions)
	}
	return nil
}
