package grid_rebalance

import (
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

// IOrderStore - 注文ストアのインターフェース
type IOrderStore interface {
	FindByCode(code string) (*Order, error)
	FindActiveOrders() ([]*Order, error)
	FindActiveOrdersByStrategyCode(strategyCode string) ([]*Order, error)
	Insert(order *Order) error
}

// orderStore - 注文ストア
type orderStore struct {
	baseStore
	store *genji.DB
}

// documentToOrder - 結果のドキュメントをOrder構造体に詰める
func (s *orderStore) documentToOrder(doc types.Document) (*Order, error) {
	var order Order
	if err := document.StructScan(doc, &order); err != nil {
		return nil, s.WrapErr(err)
	}
	return &order, nil
}

// iterateToOrderSlice - 結果をOrderスライスに詰める
func (s *orderStore) iterateToOrderSlice(res *genji.Result) ([]*Order, error) {
	result := make([]*Order, 0)
	err := res.Iterate(func(d types.Document) error {
		var order Order
		if err := document.StructScan(d, &order); err != nil {
			return err
		}
		result = append(result, &order)
		return nil
	})
	if err != nil {
		return nil, s.WrapErr(err)
	}
	return result, nil
}

// FindByCode - Orderをコードで取り出す
func (s *orderStore) FindByCode(code string) (*Order, error) {
	doc, err := s.store.QueryDocument(`select * from orders where code = ?`, code)
	if err != nil {
		return nil, s.WrapErr(err)
	}

	return s.documentToOrder(doc)
}

// FindActiveOrders - 注文中の注文を取り出す
func (s *orderStore) FindActiveOrders() ([]*Order, error) {
	res, err := s.store.Query(`select * from orders where status = 'in_order' order by code`)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToOrderSlice(res)
}

// FindActiveOrdersByStrategyCode - 戦略を指定して注文中の注文を取り出す
func (s *orderStore) FindActiveOrdersByStrategyCode(strategyCode string) ([]*Order, error) {
	res, err := s.store.Query(`select * from orders where status = 'in_order' and strategycode = ? order by code`, strategyCode)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToOrderSlice(res)
}

// Insert - データの登録
func (s *orderStore) Insert(order *Order) error {
	return s.WrapErr(s.store.Exec(`insert into orders values ?`, order))
}
