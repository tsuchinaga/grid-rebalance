package grid_rebalance

import "time"

// Account - 口座情報
type Account struct {
	Password    string      // パスワード
	AccountType AccountType // 口座種別
}

// ExitPosition - エグジットポジション
type ExitPosition struct {
	PositionCode     string  // ポジションコード
	Quantity         float64 // 数量
	ReleasedQuantity float64 // 解放済み数量
}

// OrderResult - 注文結果
type OrderResult struct {
	Result        bool      // 注文成否
	Code          string    // 注文コード
	ErrorCode     string    // エラーコード
	OrderDateTime time.Time // 注文日時
}

// HoldPosition - ポジションの拘束数量
type HoldPosition struct {
	PositionCode string  // ポジションコード
	HoldQuantity float64 // 拘束数量
}

// Symbol - 銘柄情報
type Symbol struct {
	Code             string   // 銘柄コード
	Exchange         Exchange // 市場
	TradingUnit      float64  // 売買単位
	UpperLimit       float64  // 値幅上限
	LowerLimit       float64  // 値幅加減
	CurrentPrice     float64  // 現在値
	CalculationPrice float64  // 計算用現在値
	BidPrice         float64  // 最良買い気配値
	AskPrice         float64  // 最良売り気配値
}
