package grid_rebalance

import (
	"context"
	"errors"
	"reflect"
	"testing"
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"

	"google.golang.org/grpc"

	"gitlab.com/tsuchinaga/kabus-grpc-server/kabuspb"
)

type testKabusAPI struct {
	IKabusAPI
	SendOrder1       *OrderResult
	SendOrder2       error
	SendOrderHistory []interface{}
	GetSymbol1       *Symbol
	GetSymbol2       error
	RegisterSymbol1  error
}

func (t *testKabusAPI) SendOrder(strategy *Strategy, order *Order) (*OrderResult, error) {
	if t.SendOrderHistory == nil {
		t.SendOrderHistory = make([]interface{}, 0)
	}
	t.SendOrderHistory = append(t.SendOrderHistory, strategy)
	t.SendOrderHistory = append(t.SendOrderHistory, order)
	return t.SendOrder1, t.SendOrder2
}

func (t *testKabusAPI) GetSymbol(string, Exchange) (*Symbol, error) {
	return t.GetSymbol1, t.GetSymbol2
}

func (t *testKabusAPI) RegisterSymbol(string, Exchange) error {
	return t.RegisterSymbol1
}

type testKabusServiceClient struct {
	kabuspb.KabusServiceClient
	sendStockOrder1        *kabuspb.OrderResponse
	sendStockOrder2        error
	sendStockOrderHistory  []*kabuspb.SendStockOrderRequest
	sendMarginOrder1       *kabuspb.OrderResponse
	sendMarginOrder2       error
	sendMarginOrderHistory []*kabuspb.SendMarginOrderRequest
	GetBoard1              *kabuspb.Board
	GetBoard2              error
	GetSymbol1             *kabuspb.Symbol
	GetSymbol2             error
	RegisterSymbols1       *kabuspb.RegisteredSymbols
	RegisterSymbols2       error
}

func (t *testKabusServiceClient) SendStockOrder(_ context.Context, in *kabuspb.SendStockOrderRequest, _ ...grpc.CallOption) (*kabuspb.OrderResponse, error) {
	if t.sendStockOrderHistory == nil {
		t.sendStockOrderHistory = []*kabuspb.SendStockOrderRequest{}
	}
	t.sendStockOrderHistory = append(t.sendStockOrderHistory, in)
	return t.sendStockOrder1, t.sendStockOrder2
}
func (t *testKabusServiceClient) SendMarginOrder(_ context.Context, in *kabuspb.SendMarginOrderRequest, _ ...grpc.CallOption) (*kabuspb.OrderResponse, error) {
	if t.sendMarginOrderHistory == nil {
		t.sendMarginOrderHistory = []*kabuspb.SendMarginOrderRequest{}
	}
	t.sendMarginOrderHistory = append(t.sendMarginOrderHistory, in)
	return t.sendMarginOrder1, t.sendMarginOrder2
}
func (t *testKabusServiceClient) GetBoard(context.Context, *kabuspb.GetBoardRequest, ...grpc.CallOption) (*kabuspb.Board, error) {
	return t.GetBoard1, t.GetBoard2
}
func (t *testKabusServiceClient) GetSymbol(context.Context, *kabuspb.GetSymbolRequest, ...grpc.CallOption) (*kabuspb.Symbol, error) {
	return t.GetSymbol1, t.GetSymbol2
}
func (t *testKabusServiceClient) RegisterSymbols(context.Context, *kabuspb.RegisterSymbolsRequest, ...grpc.CallOption) (*kabuspb.RegisteredSymbols, error) {
	return t.RegisterSymbols1, t.RegisterSymbols2
}

func Test_kabusAPI_sideTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 Side
		want kabuspb.Side
	}{
		{name: "未指定 を変換できる", arg1: SideUnspecified, want: kabuspb.Side_SIDE_UNSPECIFIED},
		{name: "買い を変換できる", arg1: SideBuy, want: kabuspb.Side_SIDE_BUY},
		{name: "売り を変換できる", arg1: SideSell, want: kabuspb.Side_SIDE_SELL},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.sideTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_exchangeTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 Exchange
		want kabuspb.Exchange
	}{
		{name: "未指定 を変換できる", arg1: ExchangeUnspecified, want: kabuspb.Exchange_EXCHANGE_UNSPECIFIED},
		{name: "東証 を変換できる", arg1: ExchangeToushou, want: kabuspb.Exchange_EXCHANGE_TOUSHOU},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.exchangeTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_exchangeFrom(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 kabuspb.Exchange
		want Exchange
	}{
		{name: "未指定 を変換できる", arg1: kabuspb.Exchange_EXCHANGE_UNSPECIFIED, want: ExchangeUnspecified},
		{name: "東証 を変換できる", arg1: kabuspb.Exchange_EXCHANGE_TOUSHOU, want: ExchangeToushou},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.exchangeFrom(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_accountTypeTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 AccountType
		want kabuspb.AccountType
	}{
		{name: "未指定 を変換できる", arg1: AccountTypeUnspecified, want: kabuspb.AccountType_ACCOUNT_TYPE_UNSPECIFIED},
		{name: "一般 を変換できる", arg1: AccountTypeGeneral, want: kabuspb.AccountType_ACCOUNT_TYPE_GENERAL},
		{name: "特定 を変換できる", arg1: AccountTypeSpecific, want: kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC},
		{name: "法人 を変換できる", arg1: AccountTypeCorporation, want: kabuspb.AccountType_ACCOUNT_TYPE_CORPORATION},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.accountTypeTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_orderTypeTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 ExecutionType
		want kabuspb.StockOrderType
	}{
		{name: "未指定 を変換できる", arg1: ExecutionTypeUnspecified, want: kabuspb.StockOrderType_STOCK_ORDER_TYPE_UNSPECIFIED},
		{name: "成行 を変換できる", arg1: ExecutionTypeMarket, want: kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO},
		{name: "指値 を変換できる", arg1: ExecutionTypeLimit, want: kabuspb.StockOrderType_STOCK_ORDER_TYPE_LO},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.orderTypeTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_tradeTypeTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 TradeType
		want kabuspb.TradeType
	}{
		{name: "未指定 を変換できる", arg1: TradeTypeUnspecified, want: kabuspb.TradeType_TRADE_TYPE_UNSPECIFIED},
		{name: "エントリー を変換できる", arg1: TradeTypeEntry, want: kabuspb.TradeType_TRADE_TYPE_ENTRY},
		{name: "エグジット を変換できる", arg1: TradeTypeExit, want: kabuspb.TradeType_TRADE_TYPE_EXIT},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.tradeTypeTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_marginTradeTypeTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 StockMarginTradeType
		want kabuspb.MarginTradeType
	}{
		{name: "未指定 を変換できる", arg1: StockMarginTradeTypeUnspecified, want: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_UNSPECIFIED},
		{name: "現物 を変換できる", arg1: StockMarginTradeTypeStock, want: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_UNSPECIFIED},
		{name: "制度 を変換できる", arg1: StockMarginTradeTypeMarginSystem, want: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_SYSTEM},
		{name: "長期 を変換できる", arg1: StockMarginTradeTypeMarginLong, want: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_LONG},
		{name: "デイトレ を変換できる", arg1: StockMarginTradeTypeMarginDay, want: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_DAY},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.marginTradeTypeTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_closePositionsTo(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		arg1 []ExitPosition
		want []*kabuspb.ClosePosition
	}{
		{name: "nilならnilを返す", arg1: nil, want: nil},
		{name: "空配列なら空配列を返す", arg1: []ExitPosition{}, want: []*kabuspb.ClosePosition{}},
		{name: "要素があれば置き換えて返す", arg1: []ExitPosition{
			{PositionCode: "POSITION-CODE-001", Quantity: 100},
			{PositionCode: "POSITION-CODE-002", Quantity: 150},
			{PositionCode: "POSITION-CODE-003", Quantity: 300},
		}, want: []*kabuspb.ClosePosition{
			{ExecutionId: "POSITION-CODE-001", Quantity: 100},
			{ExecutionId: "POSITION-CODE-002", Quantity: 150},
			{ExecutionId: "POSITION-CODE-003", Quantity: 300},
		}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabus := &kabusAPI{}
			got := kabus.closePositionsTo(test.arg1)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_kabusAPI_SendOrder(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name                       string
		clock                      *testClock
		kabusServiceClient         *testKabusServiceClient
		arg1                       *Strategy
		arg2                       *Order
		want1                      *OrderResult
		want2                      error
		wantSendStockOrderHistory  []*kabuspb.SendStockOrderRequest
		wantSendMarginOrderHistory []*kabuspb.SendMarginOrderRequest
	}{
		{name: "strategyがnilならnil error",
			kabusServiceClient: &testKabusServiceClient{},
			arg1:               nil,
			arg2:               &Order{},
			want2:              ErrNilArguments},
		{name: "orderがnilならnil error",
			kabusServiceClient: &testKabusServiceClient{},
			arg1:               &Strategy{},
			arg2:               nil,
			want2:              ErrNilArguments},
		{name: "現物注文でエラーが出たらエラーを返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendStockOrder1: nil,
				sendStockOrder2: ErrUnknown,
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeStock,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeStock,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want2: ErrUnknown,
			wantSendStockOrderHistory: []*kabuspb.SendStockOrderRequest{{
				Password:     "Password1234",
				SymbolCode:   "1475",
				Exchange:     kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:         kabuspb.Side_SIDE_BUY,
				DeliveryType: kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				FundType:     kabuspb.FundType_FUND_TYPE_SUBSTITUTE_MARGIN,
				AccountType:  kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:     5,
				OrderType:    kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:    timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
		{name: "現物注文で注文に成功したら注文結果を返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendStockOrder1: &kabuspb.OrderResponse{ResultCode: 0, OrderId: "ORDER-ID-001"},
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeStock,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeStock,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want1: &OrderResult{
				Result:        true,
				Code:          "ORDER-ID-001",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local),
			},
			wantSendStockOrderHistory: []*kabuspb.SendStockOrderRequest{{
				Password:     "Password1234",
				SymbolCode:   "1475",
				Exchange:     kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:         kabuspb.Side_SIDE_BUY,
				DeliveryType: kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				FundType:     kabuspb.FundType_FUND_TYPE_SUBSTITUTE_MARGIN,
				AccountType:  kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:     5,
				OrderType:    kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:    timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
		{name: "現物注文で注文に失敗したら注文結果を返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendStockOrder1: &kabuspb.OrderResponse{ResultCode: 4, OrderId: ""},
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeStock,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeStock,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want1: &OrderResult{
				Result:        false,
				Code:          "",
				ErrorCode:     "4",
				OrderDateTime: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local),
			},
			wantSendStockOrderHistory: []*kabuspb.SendStockOrderRequest{{
				Password:     "Password1234",
				SymbolCode:   "1475",
				Exchange:     kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:         kabuspb.Side_SIDE_BUY,
				DeliveryType: kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				FundType:     kabuspb.FundType_FUND_TYPE_SUBSTITUTE_MARGIN,
				AccountType:  kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:     5,
				OrderType:    kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:    timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
		{name: "信用注文でエラーが出たらエラーを返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendMarginOrder1: nil,
				sendMarginOrder2: ErrUnknown,
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want2: ErrUnknown,
			wantSendMarginOrderHistory: []*kabuspb.SendMarginOrderRequest{{
				Password:        "Password1234",
				SymbolCode:      "1475",
				Exchange:        kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:            kabuspb.Side_SIDE_BUY,
				TradeType:       kabuspb.TradeType_TRADE_TYPE_ENTRY,
				MarginTradeType: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_DAY,
				DeliveryType:    kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				AccountType:     kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:        5,
				OrderType:       kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:       timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
		{name: "現物注文で注文に成功したら注文結果を返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendMarginOrder1: &kabuspb.OrderResponse{ResultCode: 0, OrderId: "ORDER-ID-001"},
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want1: &OrderResult{
				Result:        true,
				Code:          "ORDER-ID-001",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local),
			},
			wantSendMarginOrderHistory: []*kabuspb.SendMarginOrderRequest{{
				Password:        "Password1234",
				SymbolCode:      "1475",
				Exchange:        kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:            kabuspb.Side_SIDE_BUY,
				TradeType:       kabuspb.TradeType_TRADE_TYPE_ENTRY,
				MarginTradeType: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_DAY,
				DeliveryType:    kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				AccountType:     kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:        5,
				OrderType:       kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:       timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
		{name: "現物注文で注文に失敗したら注文結果を返す",
			clock: &testClock{now1: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)},
			kabusServiceClient: &testKabusServiceClient{
				sendMarginOrder1: &kabuspb.OrderResponse{ResultCode: 4, OrderId: ""},
			},
			arg1: &Strategy{
				Code:                 "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				Account:              Account{Password: "Password1234", AccountType: AccountTypeSpecific},
			},
			arg2: &Order{
				StrategyCode:         "strategy-1475",
				SymbolCode:           "1475",
				Exchange:             ExchangeToushou,
				OrderKind:            OrderKindRebalance,
				StockMarginTradeType: StockMarginTradeTypeMarginDay,
				ExecutionType:        ExecutionTypeMarket,
				Side:                 SideBuy,
				TradeType:            TradeTypeEntry,
				OrderQuantity:        5,
				AccountType:          AccountTypeSpecific,
			},
			want1: &OrderResult{
				Result:        false,
				Code:          "",
				ErrorCode:     "4",
				OrderDateTime: time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local),
			},
			wantSendMarginOrderHistory: []*kabuspb.SendMarginOrderRequest{{
				Password:        "Password1234",
				SymbolCode:      "1475",
				Exchange:        kabuspb.StockExchange_STOCK_EXCHANGE_TOUSHOU,
				Side:            kabuspb.Side_SIDE_BUY,
				TradeType:       kabuspb.TradeType_TRADE_TYPE_ENTRY,
				MarginTradeType: kabuspb.MarginTradeType_MARGIN_TRADE_TYPE_GENERAL_DAY,
				DeliveryType:    kabuspb.DeliveryType_DELIVERY_TYPE_CASH,
				AccountType:     kabuspb.AccountType_ACCOUNT_TYPE_SPECIFIC,
				Quantity:        5,
				OrderType:       kabuspb.StockOrderType_STOCK_ORDER_TYPE_MO,
				ExpireDay:       timestamppb.New(time.Date(2021, 10, 14, 10, 0, 0, 0, time.Local)),
			}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			api := &kabusAPI{clock: test.clock, kabucom: test.kabusServiceClient}
			got1, got2 := api.SendOrder(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) ||
				!errors.Is(got2, test.want2) ||
				!reflect.DeepEqual(test.wantSendStockOrderHistory, test.kabusServiceClient.sendStockOrderHistory) ||
				!reflect.DeepEqual(test.wantSendMarginOrderHistory, test.kabusServiceClient.sendMarginOrderHistory) {
				t.Errorf("%s error\nwant: %+v, %+v, %+v, %+v\ngot: %+v, %+v, %+v, %+v\n", t.Name(),
					test.want1, test.want2, test.wantSendStockOrderHistory, test.wantSendMarginOrderHistory,
					got1, got2, test.kabusServiceClient.sendStockOrderHistory, test.kabusServiceClient.sendMarginOrderHistory)
			}
		})
	}
}

func Test_kabusAPI_GetSymbol(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name               string
		kabusServiceClient *testKabusServiceClient
		arg1               string
		arg2               Exchange
		want1              *Symbol
		want2              error
	}{
		{name: "symbol取得に失敗したらエラー",
			kabusServiceClient: &testKabusServiceClient{GetSymbol2: ErrUnknown},
			arg1:               "1475",
			arg2:               ExchangeToushou,
			want2:              ErrUnknown},
		{name: "board取得に失敗したらエラー",
			kabusServiceClient: &testKabusServiceClient{GetSymbol1: &kabuspb.Symbol{}, GetBoard2: ErrUnknown},
			arg1:               "1475",
			arg2:               ExchangeToushou,
			want2:              ErrUnknown},
		{name: "symbolもboardも取得できたら情報を返す",
			kabusServiceClient: &testKabusServiceClient{
				GetSymbol1: &kabuspb.Symbol{Code: "1475", Exchange: kabuspb.Exchange_EXCHANGE_TOUSHOU, TradingUnit: 1, UpperLimit: 2576, LowerLimit: 1576},
				GetBoard1:  &kabuspb.Board{CurrentPrice: 2076, CalculationPrice: 2076, BidPrice: 2075, AskPrice: 2077}},
			arg1: "1475",
			arg2: ExchangeToushou,
			want1: &Symbol{
				Code:             "1475",
				Exchange:         ExchangeToushou,
				TradingUnit:      1,
				UpperLimit:       2576,
				LowerLimit:       1576,
				CurrentPrice:     2076,
				CalculationPrice: 2076,
				BidPrice:         2075,
				AskPrice:         2077,
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabusapi := &kabusAPI{kabucom: test.kabusServiceClient}
			got1, got2 := kabusapi.GetSymbol(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_kabusAPI_RegisterSymbol(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name               string
		kabusServiceClient *testKabusServiceClient
		arg1               string
		arg2               Exchange
		want1              error
	}{
		{name: "銘柄登録でエラーがあればエラー",
			kabusServiceClient: &testKabusServiceClient{RegisterSymbols2: ErrUnknown},
			arg1:               "1475",
			arg2:               ExchangeToushou,
			want1:              ErrUnknown},
		{name: "銘柄登録に成功すればエラーはなし",
			kabusServiceClient: &testKabusServiceClient{},
			arg1:               "1475",
			arg2:               ExchangeToushou,
			want1:              nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			kabusapi := &kabusAPI{kabucom: test.kabusServiceClient}
			got1 := kabusapi.RegisterSymbol(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want1, got1)
			}
		})
	}
}
