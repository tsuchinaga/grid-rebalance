package grid_rebalance

import (
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
)

// IStrategyStore - 戦略ストアのインターフェース
type IStrategyStore interface {
	IsExistsByCode(code string) bool
	FindByCode(code string) (*Strategy, error)
	FindActiveStrategies() ([]*Strategy, error)
	Insert(strategy *Strategy) error
	Delete(code string) error
}

// strategyStore - 戦略ストア
type strategyStore struct {
	baseStore
	store *genji.DB
}

// documentToStrategy - 結果のドキュメントをStrategy構造体に詰める
func (s *strategyStore) documentToStrategy(doc types.Document) (*Strategy, error) {
	var strategy Strategy
	if err := document.StructScan(doc, &strategy); err != nil {
		return nil, s.WrapErr(err)
	}
	return &strategy, nil
}

// iterateToStrategySlice - 結果をStrategyスライスに詰める
func (s *strategyStore) iterateToStrategySlice(res *genji.Result) ([]*Strategy, error) {
	result := make([]*Strategy, 0)
	err := res.Iterate(func(d types.Document) error {
		var strategy Strategy
		if err := document.StructScan(d, &strategy); err != nil {
			return err
		}
		result = append(result, &strategy)
		return nil
	})
	if err != nil {
		return nil, s.WrapErr(err)
	}
	return result, nil
}

// IsExistsByCode - 指定したコードの戦略が存在するか
func (s *strategyStore) IsExistsByCode(code string) bool {
	strategy, err := s.FindByCode(code)
	if err != nil {
		return false
	}
	return strategy != nil
}

// FindByCode - Strategyをコードで取り出す
func (s *strategyStore) FindByCode(code string) (*Strategy, error) {
	doc, err := s.store.QueryDocument(`select * from strategies where code = ?`, code)
	if err != nil {
		return nil, s.WrapErr(err)
	}

	return s.documentToStrategy(doc)
}

// FindActiveStrategies - 有効な戦略を取り出す
func (s *strategyStore) FindActiveStrategies() ([]*Strategy, error) {
	res, err := s.store.Query(`select * from strategies where active = true order by code`)
	if err != nil {
		return nil, s.WrapErr(err)
	}
	defer res.Close()

	return s.iterateToStrategySlice(res)
}

// Insert - データの登録
func (s *strategyStore) Insert(strategy *Strategy) error {
	return s.WrapErr(s.store.Exec(`insert into strategies values ?`, strategy))
}

// Delete - データの削除
func (s *strategyStore) Delete(code string) error {
	return s.WrapErr(s.store.Exec("delete from strategies where code = ?", code))
}
