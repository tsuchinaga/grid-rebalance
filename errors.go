package grid_rebalance

import "errors"

var (
	ErrUnknown                 = errors.New("unknown")
	ErrNilArguments            = errors.New("nil arguments")
	ErrDataNotFound            = errors.New("data not found")
	ErrDataDuplicated          = errors.New("data duplicated")
	ErrAlreadyExists           = errors.New("already exists")
	ErrNotFound                = errors.New("not found")
	ErrNotEnoughOwnedPositions = errors.New("not enough owned positions")
	ErrStrategyValidation      = errors.New("strategy validation")
)
