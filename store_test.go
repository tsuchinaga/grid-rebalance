package grid_rebalance

import (
	"errors"
	"testing"

	gerrors "github.com/genjidb/genji/errors"
)

func Test_baseStore_WrapErr(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		arg1  error
		want1 error
	}{
		{name: "ErrDocumentNotFoundを変換できる", arg1: gerrors.ErrDocumentNotFound, want1: ErrDataNotFound},
		{name: "ErrDuplicateDocumentを変換できる", arg1: gerrors.ErrDuplicateDocument, want1: ErrDataDuplicated},
		{name: "AlreadyExistsErrorを変換できる", arg1: gerrors.AlreadyExistsError{}, want1: ErrAlreadyExists},
		{name: "NotFoundErrorを変換できる", arg1: gerrors.NotFoundError{}, want1: ErrNotFound},
		{name: "それ以外のエラーはそのまま出される", arg1: ErrUnknown, want1: ErrUnknown},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			store := &baseStore{}
			got1 := store.WrapErr(test.arg1)
			if !errors.Is(got1, test.want1) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want1, got1)
			}
		})
	}
}
