package grid_rebalance

import (
	"reflect"
	"testing"
)

func Test_service_SendRebalanceOrder(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name               string
		logger             *testLogger
		strategyService    *testStrategyService
		symbolService      *testSymbolService
		rebalanceService   *testRebalanceService
		wantWarningCount   int
		wantGetSymbolCount int
		wantRebalanceCount int
	}{
		{name: "戦略の取得に失敗したらwarningにログを残して終了",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{FindActiveStrategies2: ErrUnknown},
			symbolService:    &testSymbolService{},
			rebalanceService: &testRebalanceService{},
			wantWarningCount: 1},
		{name: "戦略がなければ何もせずに終了",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{FindActiveStrategies1: []*Strategy{}},
			symbolService:    &testSymbolService{},
			rebalanceService: &testRebalanceService{},
			wantWarningCount: 0},
		{name: "銘柄の情報取得に失敗したらwarningにログを残してスキップ",
			logger: &testLogger{},
			strategyService: &testStrategyService{FindActiveStrategies1: []*Strategy{
				{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou},
				{Code: "strategy-1476", SymbolCode: "1476", Exchange: ExchangeToushou},
				{Code: "strategy-1699", SymbolCode: "1699", Exchange: ExchangeToushou},
			}},
			symbolService:      &testSymbolService{GetSymbol2: ErrUnknown},
			rebalanceService:   &testRebalanceService{},
			wantWarningCount:   3,
			wantGetSymbolCount: 3},
		{name: "リバランス注文に失敗したらwarningにログを残してスキップ",
			logger: &testLogger{},
			strategyService: &testStrategyService{FindActiveStrategies1: []*Strategy{
				{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou},
				{Code: "strategy-1476", SymbolCode: "1476", Exchange: ExchangeToushou},
				{Code: "strategy-1699", SymbolCode: "1699", Exchange: ExchangeToushou},
			}},
			symbolService:      &testSymbolService{GetSymbol1: &Symbol{Code: "1475", BidPrice: 2000, AskPrice: 2001}},
			rebalanceService:   &testRebalanceService{Rebalance1: ErrUnknown},
			wantWarningCount:   3,
			wantGetSymbolCount: 3,
			wantRebalanceCount: 3},
		{name: "リバランス注文まで成功すればwarningにログを残さず終了",
			logger: &testLogger{},
			strategyService: &testStrategyService{FindActiveStrategies1: []*Strategy{
				{Code: "strategy-1475", SymbolCode: "1475", Exchange: ExchangeToushou},
				{Code: "strategy-1476", SymbolCode: "1476", Exchange: ExchangeToushou},
				{Code: "strategy-1699", SymbolCode: "1699", Exchange: ExchangeToushou},
			}},
			symbolService:      &testSymbolService{GetSymbol1: &Symbol{Code: "1475", BidPrice: 2000, AskPrice: 2001}},
			rebalanceService:   &testRebalanceService{Rebalance1: nil},
			wantWarningCount:   0,
			wantGetSymbolCount: 3,
			wantRebalanceCount: 3},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &service{
				logger:           test.logger,
				strategyService:  test.strategyService,
				symbolService:    test.symbolService,
				rebalanceService: test.rebalanceService,
			}
			service.SendRebalanceOrder()
			if !reflect.DeepEqual(test.wantWarningCount, test.logger.WarningCount) ||
				!reflect.DeepEqual(test.wantGetSymbolCount, test.symbolService.GetSymbolCount) ||
				!reflect.DeepEqual(test.wantRebalanceCount, test.rebalanceService.RebalanceCount) {
				t.Errorf("%s error\nwant: %+v, %+v, %+v\ngot: %+v, %+v%+v\n", t.Name(),
					test.wantWarningCount, test.wantGetSymbolCount, test.wantRebalanceCount,
					test.logger.WarningCount, test.symbolService.GetSymbolCount, test.rebalanceService.RebalanceCount)
			}
		})
	}
}

func Test_service_RegisterStrategy(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name             string
		logger           *testLogger
		strategyService  *testStrategyService
		arg1             RegisterStrategy
		wantWarningCount int
	}{
		{name: "登録に成功したらエラーログはなし",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{Register1: nil, ConvertStrategy1: &Strategy{Code: "strategy-1475"}},
			arg1:             RegisterStrategy{Code: "strategy-1475"},
			wantWarningCount: 0},
		{name: "登録に失敗したらエラーログを残す",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{Register1: ErrStrategyValidation, ConvertStrategy1: &Strategy{Code: "strategy-1475"}},
			arg1:             RegisterStrategy{Code: "strategy-1475"},
			wantWarningCount: 1},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &service{logger: test.logger, strategyService: test.strategyService}
			service.RegisterStrategy(test.arg1)
			if !reflect.DeepEqual(test.wantWarningCount, test.logger.WarningCount) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.wantWarningCount, test.logger.WarningCount)
			}
		})
	}
}

func Test_service_RemoveStrategy(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name             string
		logger           *testLogger
		strategyService  *testStrategyService
		arg1             string
		wantWarningCount int
	}{
		{name: "削除に成功したらエラーログはなし",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{Remove1: nil},
			arg1:             "strategy-1475",
			wantWarningCount: 0},
		{name: "削除に失敗したらエラーログを残す",
			logger:           &testLogger{},
			strategyService:  &testStrategyService{Remove1: ErrDataNotFound},
			arg1:             "strategy-1475",
			wantWarningCount: 1},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &service{logger: test.logger, strategyService: test.strategyService}
			service.RemoveStrategy(test.arg1)
			if !reflect.DeepEqual(test.wantWarningCount, test.logger.WarningCount) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.wantWarningCount, test.logger.WarningCount)
			}
		})
	}
}
