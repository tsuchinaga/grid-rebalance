package grid_rebalance

import (
	"errors"
	"reflect"
	"testing"
)

type testSymbolService struct {
	ISymbolService
	GetSymbol1       *Symbol
	GetSymbol2       error
	GetSymbolCount   int
	GetSymbolHistory []interface{}
}

func (t *testSymbolService) GetSymbol(symbolCode string, exchange Exchange) (*Symbol, error) {
	if t.GetSymbolHistory == nil {
		t.GetSymbolHistory = make([]interface{}, 0)
	}
	t.GetSymbolHistory = append(t.GetSymbolHistory, symbolCode)
	t.GetSymbolHistory = append(t.GetSymbolHistory, exchange)
	t.GetSymbolCount++
	return t.GetSymbol1, t.GetSymbol2
}

func Test_symbolService_GetSymbol(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		kabusapi *testKabusAPI
		arg1     string
		arg2     Exchange
		want1    *Symbol
		want2    error
	}{
		{name: "apiがエラーを返したらエラーを返す",
			kabusapi: &testKabusAPI{GetSymbol2: ErrUnknown},
			arg1:     "1475",
			arg2:     ExchangeToushou,
			want2:    ErrUnknown},
		{name: "apiが銘柄情報を返したら銘柄情報を返す",
			kabusapi: &testKabusAPI{GetSymbol1: &Symbol{
				Code:             "1475",
				Exchange:         ExchangeToushou,
				TradingUnit:      1,
				UpperLimit:       2576,
				LowerLimit:       1576,
				CurrentPrice:     2076,
				CalculationPrice: 2076,
				BidPrice:         2075,
				AskPrice:         2077,
			}},
			arg1: "1475",
			arg2: ExchangeToushou,
			want1: &Symbol{
				Code:             "1475",
				Exchange:         ExchangeToushou,
				TradingUnit:      1,
				UpperLimit:       2576,
				LowerLimit:       1576,
				CurrentPrice:     2076,
				CalculationPrice: 2076,
				BidPrice:         2075,
				AskPrice:         2077,
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &symbolService{kabusAPI: test.kabusapi}
			got1, got2 := service.GetSymbol(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) || !errors.Is(got2, test.want2) {
				t.Errorf("%s error\nwant: %+v, %+v\ngot: %+v, %+v\n", t.Name(), test.want1, test.want2, got1, got2)
			}
		})
	}
}

func Test_symbolService_RegisterSymbol(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		kabusapi *testKabusAPI
		arg1     string
		arg2     Exchange
		want1    error
	}{
		{name: "apiがerrorを返したらerr",
			kabusapi: &testKabusAPI{RegisterSymbol1: ErrUnknown},
			arg1:     "1475",
			arg2:     ExchangeToushou,
			want1:    ErrUnknown},
		{name: "apiがerrorを返さなければerrでない",
			kabusapi: &testKabusAPI{RegisterSymbol1: nil},
			arg1:     "1475",
			arg2:     ExchangeToushou,
			want1:    nil},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			service := &symbolService{kabusAPI: test.kabusapi}
			got1 := service.RegisterSymbol(test.arg1, test.arg2)
			if !reflect.DeepEqual(test.want1, got1) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want1, got1)
			}
		})
	}
}
