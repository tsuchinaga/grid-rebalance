package grid_rebalance

// OrderKind - 注文種別
type OrderKind string

const (
	OrderKindUnspecified OrderKind = ""          // 未指定
	OrderKindRebalance   OrderKind = "rebalance" // リバランス
	OrderKindGrid        OrderKind = "grid"      // グリッド
)

// OrderStatus - 注文状態
type OrderStatus string

const (
	OrderStatusUnspecified OrderStatus = ""          // 未指定
	OrderStatusInOrder     OrderStatus = "in_order"  // 注文中
	OrderStatusDone        OrderStatus = "done"      // 約定済み
	OrderStatusInCancel    OrderStatus = "in_cancel" // 取消中
	OrderStatusCanceled    OrderStatus = "canceled"  // 取消済み
	OrderStatusError       OrderStatus = "error"     // エラー
)

// Side - 売買方向
type Side string

const (
	SideUnspecified Side = ""     // 未指定
	SideBuy         Side = "buy"  // 買い
	SideSell        Side = "sell" // 売り
)

// Turn - 反対方向を取得する
func (e Side) Turn() Side {
	switch e {
	case SideBuy:
		return SideSell
	case SideSell:
		return SideBuy
	}
	return SideUnspecified
}

// AccountType - 口座種別
type AccountType string

const (
	AccountTypeUnspecified AccountType = ""            // 未指定
	AccountTypeGeneral     AccountType = "general"     // 一般
	AccountTypeSpecific    AccountType = "specific"    // 特定
	AccountTypeCorporation AccountType = "corporation" // 特定
)

// Exchange - 市場
type Exchange string

const (
	ExchangeUnspecified Exchange = ""        // 未指定
	ExchangeToushou     Exchange = "toushou" // 東証
)

// ExecutionType - 執行条件
type ExecutionType string

const (
	ExecutionTypeUnspecified ExecutionType = ""       // 未指定
	ExecutionTypeMarket      ExecutionType = "market" // 成行
	ExecutionTypeLimit       ExecutionType = "limit"  // 指値
)

// StockMarginTradeType - 現物・信用の取引区分
type StockMarginTradeType string

const (
	StockMarginTradeTypeUnspecified  StockMarginTradeType = ""       // 未指定
	StockMarginTradeTypeStock        StockMarginTradeType = "stock"  // 現物
	StockMarginTradeTypeMarginSystem StockMarginTradeType = "system" // 制度信用
	StockMarginTradeTypeMarginLong   StockMarginTradeType = "long"   // 一般信用長期
	StockMarginTradeTypeMarginDay    StockMarginTradeType = "day"    // 一般信用1日
)

// TradeType - 取引種別
type TradeType string

const (
	TradeTypeUnspecified TradeType = ""      // 未指定
	TradeTypeEntry       TradeType = "entry" // エントリー
	TradeTypeExit        TradeType = "exit"  // エグジット
)

// HoldOrder - 拘束順序
type HoldOrder string

const (
	HoldOrderUnspecified HoldOrder = ""       // 未指定
	HoldOrderFirst       HoldOrder = "first"  // 古いもの順
	HoldOrderLatest      HoldOrder = "latest" // 新しいもの順
)
