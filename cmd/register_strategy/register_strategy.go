package main

import gr "gitlab.com/tsuchinaga/grid-rebalance"

func main() {
	service := gr.NewService()
	service.RegisterStrategy(gr.RegisterStrategy{
		Code:                 "strategy-1476",
		SymbolCode:           "1476",
		Exchange:             gr.ExchangeToushou,
		StockMarginTradeType: gr.StockMarginTradeTypeStock,
		EntrySide:            gr.SideBuy,
		GridWidth:            2,
		GridNum:              5,
		Quantity:             2,
		AllottedAsset:        100_000,
		OrderPassword:        "Password1234",
		AccountType:          gr.AccountTypeSpecific,
	})
}
