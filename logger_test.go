package grid_rebalance

type testLogger struct {
	ILogger
	WarningCount   int
	WarningHistory []interface{}
}

func (t *testLogger) Warning(v ...interface{}) {
	if t.WarningHistory == nil {
		t.WarningHistory = make([]interface{}, 0)
	}
	t.WarningHistory = append(t.WarningHistory, v)
	t.WarningCount++
}
