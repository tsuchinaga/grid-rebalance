package grid_rebalance

// RegisterStrategy - 戦略登録リクエスト
type RegisterStrategy struct {
	Code                 string               // ID
	SymbolCode           string               // 銘柄コード
	Exchange             Exchange             // 市場
	StockMarginTradeType StockMarginTradeType // 現物・信用取引区分
	EntrySide            Side                 // エントリー時の売買方向
	GridWidth            int                  // グリッド幅
	GridNum              int                  // グリッド数(片側)
	Quantity             float64              // 1グリッドの注文数
	AllottedAsset        float64              // 割当資産額 ユーザーが設定した金額で変動しない
	OrderPassword        string               // パスワード
	AccountType          AccountType          // 口座種別
}
