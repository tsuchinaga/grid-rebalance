package grid_rebalance

import (
	"reflect"
	"testing"
)

func Test_Side_Turn(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		side Side
		want Side
	}{
		{name: "未指定 なら未指定", side: SideUnspecified, want: SideUnspecified},
		{name: "買い なら売り", side: SideBuy, want: SideSell},
		{name: "売り なら買い", side: SideSell, want: SideBuy},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got := test.side.Turn()
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}
