package grid_rebalance

import (
	"reflect"
	"testing"
	"time"
)

func Test_Order_SetOrderResult(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		arg   *OrderResult
		want  *Order
	}{
		{name: "引数がnilなら何もしない",
			order: &Order{},
			arg:   nil,
			want:  &Order{}},
		{name: "注文が成功のとき、注文の結果をOrderに反映する",
			order: &Order{},
			arg: &OrderResult{
				Result:        true,
				Code:          "ORDER-CODE-0001",
				ErrorCode:     "0",
				OrderDateTime: time.Date(2021, 10, 13, 9, 10, 0, 0, time.Local),
			},
			want: &Order{Code: "ORDER-CODE-0001", Status: OrderStatusInOrder, ErrorCode: "0", OrderDateTime: time.Date(2021, 10, 13, 9, 10, 0, 0, time.Local)}},
		{name: "注文が失敗のとき、注文の結果をOrderに反映する",
			order: &Order{},
			arg: &OrderResult{
				Result:        false,
				Code:          "ORDER-CODE-0001",
				ErrorCode:     "100",
				OrderDateTime: time.Date(2021, 10, 13, 9, 10, 0, 0, time.Local),
			},
			want: &Order{Code: "ORDER-CODE-0001", Status: OrderStatusError, ErrorCode: "100", OrderDateTime: time.Date(2021, 10, 13, 9, 10, 0, 0, time.Local)}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.order.SetOrderResult(test.arg)
			if !reflect.DeepEqual(test.want, test.order) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.order)
			}
		})
	}
}

func Test_Order_GetHoldingPositions(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		want  []HoldPosition
	}{
		{name: "ExitPositionsがnilなら空配列",
			order: &Order{},
			want:  []HoldPosition{}},
		{name: "ExitPositionsが空配列なら空配列",
			order: &Order{ExitPositions: []ExitPosition{}},
			want:  []HoldPosition{}},
		{name: "ExitPositionsにある要素で拘束中の数量を返す",
			order: &Order{ExitPositions: []ExitPosition{
				{PositionCode: "pos-code-1", Quantity: 100, ReleasedQuantity: 0},
				{PositionCode: "pos-code-2", Quantity: 100, ReleasedQuantity: 50},
				{PositionCode: "pos-code-3", Quantity: 100, ReleasedQuantity: 100},
				{PositionCode: "pos-code-4", Quantity: 100, ReleasedQuantity: 120},
			}},
			want: []HoldPosition{
				{PositionCode: "pos-code-1", HoldQuantity: 100},
				{PositionCode: "pos-code-2", HoldQuantity: 50},
			}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got := test.order.GetHoldingPositions()
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}
